# Unstable Unicorns

A web implementation of the board game "Unstable Unicorns"

------------------------------------------------------------

### Development
#### Preqreuisites:
- install npm version 16+ (https://nodejs.org/en/)
- install yarn (`npm install --global yarn`)
- navigate to the root directory and install the required packages by typing `yarn install`
#### Frontend
You can start the frontend by navigation to /packages/frontend and typing `yarn vite`
#### Backend
You can start the backend by navigation to /packages/backend and typing `yarn start`

------------------------------------------------------------
## Technical information

### Database:
Mongodb cluster name: unicorn-cluster \
Hosted at cloud.mongodb.com https://cloud.mongodb.com/v2/61449192741aad77a8dfe02e#clusters

### Redis:
Hosted on redislabs https://app.redislabs.com

### CI
We use Gitlab CI: https://gitlab.com/Arthur944/unstable-unicorns/-/pipelines

### Continuous Deployment

#### Frontend
A build for the frontend is generated and deployed automatically for each commit using netlify.
The main branch is hosted at https://peaceful-bassi-5f9ad9.netlify.app. The deployments of individual branches
are linked on gitlab and are available from the merge request's page.
More information about deployments are available at netlify's website for logged in users at: 
https://app.netlify.com/sites/peaceful-bassi-5f9ad9/overview

#### Backend
A build for the backend is generated and deployed to heroku each time new code is pushed to the corresponding remote.
(https://git.heroku.com/unstable-unicorns-elte.git). To push to this remote one needs to install the heroku cli (https://devcenter.heroku.com/articles/heroku-cli) and 
log in with a heroku user that has the necessary permissions. Once logged in, more information about the deployments is available at 
https://dashboard.heroku.com/apps/unstable-unicorns-elte
