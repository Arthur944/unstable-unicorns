import path from "path";
import { getConfigFactory } from "../../webpack.config";

export default getConfigFactory({
  entry: "./src/index.ts",
  outputOptions: {
    filename: "index.js",
    path: path.resolve(__dirname, "dist"),
  },
  isTargetingNode: true,
});
