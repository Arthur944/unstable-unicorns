import { GameBoard } from "../../GameEnvironment/GameBoard";
import { GameManager } from "../../GameEnvironment/GameManager";
import { CreatureCard, CreatureSubType, CreatureType } from "../CreatureCard";
import { Player } from "../../GameEnvironment/Player";
import { MagicCard, MagicType } from "../MagicCard";
import { GradeType, ModifierCard } from "../ModifierCard";

export function createBasicTestResources() {
  const gameBoard = new GameBoard();
  const gameManager = new GameManager(gameBoard);
  return {
    gameBoard,
    gameManager,
  };
}

export function createCardTestResources() {
  const { gameManager } = createBasicTestResources();
  const babyNarwhal = new CreatureCard(
    "01",
    gameManager,
    "Baby Narwhal",
    "",
    "goes to nursery",
    CreatureType.UNICORN,
    CreatureSubType.BABY
  );
  const narwhal = new CreatureCard("02", gameManager, "Narwhal", "", "", CreatureType.UNICORN, CreatureSubType.BASIC);
  const winUnicorn = new CreatureCard(
    "03",
    gameManager,
    "The wining Unicorn",
    "",
    "This card can do so many things you will always win",
    CreatureType.UNICORN,
    CreatureSubType.MAGICAL
  );
  const normalMagicCard = new MagicCard("10", gameManager, "Normal Magic Card", "", "nothing special", MagicType.MAGIC);
  const specialMagicCard = new MagicCard("11", gameManager, "Special Magic Card", "", "very special", MagicType.MAGIC);
  const upgradeCard = new ModifierCard("20", gameManager, "Upgrade Card", "", "", GradeType.UPGRADE);
  const downgradeCard = new ModifierCard("21", gameManager, "Downgrade Card", "", "", GradeType.DOWNGRADE);

  return {
    babyNarwhal,
    narwhal,
    winUnicorn,
    normalMagicCard,
    specialMagicCard,
    upgradeCard,
    downgradeCard,
  };
}

export function createGameBoardTestResources() {
  const { gameBoard } = createBasicTestResources();
  return {
    gameBoard,
    gameManager: new GameManager(gameBoard),
    players: new Array(9).fill(0).map((elem, index) => new Player(`p${index}`)),
  };
}
