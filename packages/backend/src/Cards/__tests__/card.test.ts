import { GameBoard } from "../../GameEnvironment/GameBoard";
import { GameManager } from "../../GameEnvironment/GameManager";
import { CreatureCard, CreatureSubType, CreatureType } from "../CreatureCard";
import { createCardTestResources, createGameBoardTestResources } from "./test-resources";
import { Player } from "../../GameEnvironment/Player";

test("create new card", () => {
  const { babyNarwhal } = createCardTestResources();
  expect(babyNarwhal.getName()).toStrictEqual("Baby Narwhal");
});

test("set CreatureType", () => {
  const { babyNarwhal, narwhal, winUnicorn } = createCardTestResources();
  [babyNarwhal, narwhal, winUnicorn].forEach((creature) => creature.setType(CreatureType.PANDA));
  narwhal.setType(CreatureType.UNICORN);
  expect([babyNarwhal, narwhal, winUnicorn].map((creature) => creature.getCreatureType())).toStrictEqual([
    CreatureType.PANDA,
    CreatureType.UNICORN,
    CreatureType.PANDA,
  ]);
});

test("compare two cards", () => {
  const { babyNarwhal, narwhal } = createCardTestResources();
  const gameBoard = new GameBoard();
  const gameMaster = new GameManager(gameBoard);
  expect(babyNarwhal.isEqual(babyNarwhal)).toBe(true);
  expect(babyNarwhal.isEqual(narwhal)).toBe(false);
  expect(narwhal.isEqual(babyNarwhal)).toBe(false);
  expect(
    narwhal.isEqual(new CreatureCard("02", gameMaster, "Narwhal", "", "", CreatureType.UNICORN, CreatureSubType.BASIC))
  ).toBe(true);
});

test("getActivePlayer undefined", () => {
  const { gameBoard, gameManager, players } = createGameBoardTestResources();
  const { winUnicorn } = createCardTestResources();
  winUnicorn.setUnicornValue(8);
  players.forEach((player) => gameBoard.addPlayer(player));
  winUnicorn.enterStable();
  expect(gameManager.getActivePlayer()).toBeUndefined();
});

test("gameBoard.getPlayers returns players", () => {
  const { gameBoard, players } = createGameBoardTestResources();
  players.forEach((player) => gameBoard.addPlayer(player));
  expect(gameBoard.getPlayers()[0]).toBeInstanceOf(Player);
});
