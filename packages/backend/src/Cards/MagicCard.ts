import { Card, CardType } from "./Card";
import { GameManager } from "../GameEnvironment/GameManager";

export enum MagicType {
  MAGIC,
}

export class MagicCard extends Card {
  type = CardType.MAGIC;
  private magicType: MagicType;

  public constructor(
    id: string,
    gameManager: GameManager,
    name: string,
    image: string,
    description: string,
    type: MagicType
  ) {
    super(id, gameManager, name, image, description);
    this.magicType = type;
  }

  public getMagicType(): MagicType {
    return this.magicType;
  }
}
