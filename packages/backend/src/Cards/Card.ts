import { GameManager } from "../GameEnvironment/GameManager";
import { EventType, GameBoardEvent } from "../Events/GameBoardEvent";
import { GameManagerReportTicket } from "../GameEnvironment/GameManagerReportTicket";
import { Player } from "../GameEnvironment/Player";

export enum CardType {
  CREATURE = "creature",
  INSTANT = "instant",
  MAGIC = "magic",
  MODIFIER = "modifier",
}

export abstract class Card {
  private id: string;
  private owner: Player | null;
  private name: string;
  private image: string;
  private description: string;
  private events: GameBoardEvent[];
  protected gameManager: GameManager;
  protected abstract type: CardType;

  protected constructor(id: string, gameManager: GameManager, name: string, image: string, description: string) {
    this.id = id;
    this.owner = null;
    this.name = name;
    this.image = image;
    this.description = description;
    this.events = [];
    this.gameManager = gameManager;
  }

  public getId(): string {
    return this.id;
  }

  public getOwner(): Player | null {
    return this.owner;
  }

  public getName(): string {
    return this.name;
  }

  public getImage(): string {
    return this.image;
  }

  public getDescription(): string {
    return this.description;
  }

  public getEvents(): GameBoardEvent[] {
    return [...this.events];
  }

  public setOwner(player: Player | null): void {
    this.owner = player;
  }

  public addGameBoardEvent(event: GameBoardEvent): void {
    this.events.push(event);
  }

  public play(): void {
    this.sendReportTicket(EventType.PLAY);
  }

  public discard(): void {
    this.sendReportTicket(EventType.DISCARD);
  }

  protected sendReportTicket(eventType: EventType): void {
    const reportTicket: GameManagerReportTicket = new GameManagerReportTicket(this, eventType);

    this.gameManager.receiveReportTicket(reportTicket);
  }

  public getType(): CardType {
    return this.type;
  }

  isEqual(other: Card) {
    return other.id === this.id;
  }
}
