import { StableCard } from "./StableCard";
import { GameManager } from "../GameEnvironment/GameManager";
import { CardType } from "./Card";

export enum CreatureType {
  UNICORN,
  PANDA,
}

export enum CreatureSubType {
  BABY,
  BASIC,
  MAGICAL,
}

export class CreatureCard extends StableCard {
  type = CardType.CREATURE;
  private creatureType: CreatureType;
  private subType: CreatureSubType;
  private unicornValue: number;

  public constructor(
    id: string,
    gameManager: GameManager,
    name: string,
    image: string,
    description: string,
    type: CreatureType,
    subType: CreatureSubType
  ) {
    super(id, gameManager, name, image, description);
    this.creatureType = type;
    this.subType = subType;
    this.unicornValue = 1;
  }

  public getCreatureType(): CreatureType {
    return this.creatureType;
  }

  public getSubType(): CreatureSubType {
    return this.subType;
  }

  public getUnicornValue(): number {
    return this.unicornValue;
  }

  public setType(type: CreatureType): void {
    this.creatureType = type;
  }

  public setUnicornValue(value: number): void {
    this.unicornValue = value;
  }
}
