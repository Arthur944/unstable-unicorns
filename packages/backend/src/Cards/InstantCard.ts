import { Card, CardType } from "./Card";
import { GameManager } from "../GameEnvironment/GameManager";

export enum InstantType {
  NEIGHT,
  SUPER_NEIGH,
}

export class InstantCard extends Card {
  type = CardType.INSTANT;
  private instantType: InstantType;

  public constructor(
    id: string,
    gameManager: GameManager,
    name: string,
    image: string,
    description: string,
    type: InstantType
  ) {
    super(id, gameManager, name, image, description);
    this.instantType = type;
  }

  public getInstantType(): InstantType {
    return this.instantType;
  }
}
