import { Card } from "./Card";
import { EventType } from "../Events/GameBoardEvent";

export abstract class StableCard extends Card {
  public enterStable(): void {
    this.sendReportTicket(EventType.ENTER_STABLE);
  }

  public leaveStable(): void {
    this.sendReportTicket(EventType.LEAVE_STABLE);
  }

  public destroy(): void {
    this.sendReportTicket(EventType.DESTROY);
  }

  public sacrifice(): void {
    this.sendReportTicket(EventType.SACRIFICE);
  }

  public returnToHand(): void {
    this.sendReportTicket(EventType.RETURN_TO_HAND);
  }
}
