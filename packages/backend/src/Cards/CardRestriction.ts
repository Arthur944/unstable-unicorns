import { CreatureCard, CreatureSubType, CreatureType } from "./CreatureCard";
import { GradeType, ModifierCard } from "./ModifierCard";
import { Card, CardType } from "./Card";
import { MagicCard, MagicType } from "./MagicCard";
import { InstantCard, InstantType } from "./InstantCard";

export class CardRestriction {
  private creatureTypeRestriction: Set<CreatureType> = new Set();
  private creatureSubTypeRestriction: Set<CreatureSubType> = new Set();
  private modifierRestriction: Set<GradeType> = new Set();
  private instantRestriction: Set<InstantType> = new Set();
  private magicRestriction: Set<MagicType> = new Set();

  public addCreatureTypeRestriction(restriction: CreatureType): void {
    this.creatureTypeRestriction.add(restriction);
  }

  public addCreatureSubTypeRestriction(restriction: CreatureSubType): void {
    this.creatureSubTypeRestriction.add(restriction);
  }

  public addModifierRestriction(restriction: GradeType): void {
    this.modifierRestriction.add(restriction);
  }

  public addMagicTypeRestriction(restriction: MagicType): void {
    this.magicRestriction.add(restriction);
  }

  public addInstantTypeRestriction(restriction: InstantType): void {
    this.instantRestriction.add(restriction);
  }

  public removeCreatureTypeRestriction(restriction: CreatureType): void {
    this.creatureTypeRestriction.delete(restriction);
  }

  public removeCreatureSubTypeRestriction(restriction: CreatureSubType): void {
    this.creatureSubTypeRestriction.delete(restriction);
  }

  public removeModifierRestriction(restriction: GradeType): void {
    this.modifierRestriction.delete(restriction);
  }

  public removeMagicTypeRestriction(restriction: MagicType): void {
    this.magicRestriction.delete(restriction);
  }

  public removeInstantTypeRestriction(restriction: InstantType): void {
    this.instantRestriction.delete(restriction);
  }

  public checkCard(card: Card): boolean {
    switch (card.getType()) {
      case CardType.CREATURE:
        const type = (card as CreatureCard).getCreatureType();
        const subType = (card as CreatureCard).getSubType();
        return this.creatureTypeRestriction.has(type) && this.creatureSubTypeRestriction.has(subType);
      case CardType.MODIFIER:
        return this.modifierRestriction.has((card as ModifierCard).getGradeType());
      case CardType.MAGIC:
        return this.magicRestriction.has((card as MagicCard).getMagicType());
      case CardType.INSTANT:
        return this.instantRestriction.has((card as InstantCard).getInstantType());
    }
    throw new Error("Unknown card type encountered: " + card.getType());
  }
}
