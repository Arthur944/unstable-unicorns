import { StableCard } from "./StableCard";
import { GameManager } from "../GameEnvironment/GameManager";
import { CardType } from "./Card";

export enum GradeType {
  UPGRADE,
  DOWNGRADE,
}

export class ModifierCard extends StableCard {
  type = CardType.MODIFIER;
  private gradeType: GradeType;

  public constructor(
    id: string,
    gameManager: GameManager,
    name: string,
    image: string,
    description: string,
    type: GradeType
  ) {
    super(id, gameManager, name, image, description);
    this.gradeType = type;
  }

  public getGradeType(): GradeType {
    return this.gradeType;
  }
}
