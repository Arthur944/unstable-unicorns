import { LitbaseServer } from "@litbase/server";
import path from "path";

export const server = new LitbaseServer({
  fileStoragePath: path.resolve(process.env.UPLOAD_PATH || path.join(__dirname, "../uploads")),
  enableFileServer: true,
});
