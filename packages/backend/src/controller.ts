import { rpcCallable } from "@litbase/core";
import { ServerRpcInterface } from "@litbase/server";
import { ControllerInterface } from "@unstable-unicorns/common/controllers/controller-interface";
import { Game } from "./GameEnvironment/Game";
import { Player } from "./GameEnvironment/Player";

export class Controller implements ServerRpcInterface<ControllerInterface> {
  private game: Game = new Game();

  @rpcCallable()
  // @ts-ignore
  public async doSomething() {
    return null;
  }

  private getCurrentGame() {
    return this.game;
  }

  public async addPlayer(player: Player) {
    this.getCurrentGame().getGameBoard().addPlayer(player);
  }

  public async removePlayer(player: Player) {
    this.getCurrentGame().getGameBoard().removePlayer(player);
  }

  public async nextTurn() {
    this.getCurrentGame().getGameManager().nextTurn();
  }

  public async playCardFromHand(player: Player, index: number) {
    player.playCardFromHand(index);
  }
}
