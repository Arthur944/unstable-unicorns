import util from "util";
import { LitQL } from "@litbase/core";
import { MongoDatabase } from "@litbase/server";
import { isSingular, plural, singular } from "pluralize";
import { Logger } from "mongodb";
import { Collections } from "@unstable-unicorns/common/models/collections";

const mongodbUrl = process.env.MONGO_URL as string;
const mongodbDatabase = process.env.MONGO_DATABASE as string;

Logger.setLevel("debug");

export const database = new MongoDatabase({
  address: mongodbUrl,
  database: mongodbDatabase,
});

database.ready.catch((err) => console.error(util.inspect(err, { colors: true, depth: 5 })));

export const usersCollection = database.getCollection(Collections.USERS);
export const gamesCollection = database.getCollection(Collections.GAMES);

export function registerModels(database: MongoDatabase, litql: LitQL) {
  litql.registerTypeLookup((name) => {
    let singularName, pluralName: string;

    if (isSingular(name)) {
      singularName = name;
      pluralName = plural(name);
    } else {
      singularName = singular(name);
      pluralName = name;
    }

    return litql.createCollectionType({
      name: singularName,
      plural: pluralName,
      idField: "_id",
      collection: database.getCollection(pluralName),
    });
  });
  litql.registerCollection({
    name: Collections.USERS,
    plural: "users",
    idField: "_id",
    collection: usersCollection,
  });
  litql.registerCollection({
    name: Collections.GAMES,
    plural: "games",
    idField: "_id",
    collection: gamesCollection,
  });

  litql.registerJoin(
    {
      type: Collections.GAMES,
      field: "creator",
      joinField: "creatorId",
      joinType: "many",
    },
    {
      type: Collections.USERS,
      field: "games",
      joinField: "_id",
      joinType: "one",
    }
  );

  litql.registerJoin(
    {
      type: Collections.HANDS,
      field: "cards",
      joinField: "cardIds",
      joinType: "many",
    },
    {
      type: Collections.CARDS,
      field: "hands",
      joinField: "_id",
      joinType: "many",
    }
  );
}
