import { CreatureCard, CreatureType } from "../Cards/CreatureCard";
import { ModifierCard } from "../Cards/ModifierCard";
import { CardType } from "../Cards/Card";
import { Player } from "./Player";

export class Stable {
  private owner: Player;
  private creatures: CreatureCard[] = [];
  private modifiers: ModifierCard[] = [];

  public constructor(owner: Player) {
    this.owner = owner;
  }

  public getCreatures(): CreatureCard[] {
    return [...this.creatures];
  }

  public getModifiers() {
    return [...this.modifiers];
  }

  public addCard(card: CreatureCard | ModifierCard): void {
    if (card.getType() === CardType.CREATURE) {
      const creature = card as CreatureCard;
      this.creatures.push(creature);
      creature.enterStable();
    } else {
      const modifier = card as ModifierCard;
      this.modifiers.push(modifier);
      modifier.enterStable();
    }

    card.setOwner(this.owner);
  }

  public removeCard(card: CreatureCard | ModifierCard): void {
    if (card.getType() === CardType.CREATURE) {
      this.creatures = this.creatures.filter((creature) => !creature.isEqual(card));
    } else {
      this.modifiers = this.modifiers.filter((modifier) => modifier.isEqual(card));
    }

    card.setOwner(null);
    card.leaveStable();
  }

  public countUnicorns(): number {
    let ret = 0;
    const unicorns = this.creatures.filter((elem) => elem.getCreatureType() === CreatureType.UNICORN);

    unicorns.map((elem) => (ret += elem.getUnicornValue()));

    return ret;
  }

  public getCardIDs(): string[] {
    const cardIDs: string[] = [];
    this.creatures.forEach((elem) => cardIDs.push(elem.getId()));
    this.modifiers.forEach((elem) => cardIDs.push(elem.getId()));
    return cardIDs;
  }
}
