import { GameBoard } from "../GameBoard";
import { Player } from "../Player";
import { createGameBoardTestResources } from "../../Cards/__tests__/test-resources";

test("add one player too often", () => {
  const { gameBoard, players } = createGameBoardTestResources();
  players.slice(0, 8).map((player) => {
    gameBoard.addPlayer(player);
  });
  expect(gameBoard.getPlayers()).toStrictEqual(players.slice(0, 8));
  expect(gameBoard.isBoardFull()).toBe(true);
});

test("add too many different players", () => {
  const gameBoard = new GameBoard();

  ["p0", "p1", "p2", "p3", "p4", "p1", "p6", "p7", "p8", "p9", "p5"].forEach((p) => gameBoard.addPlayer(new Player(p)));
  expect(gameBoard.getPlayers()).toStrictEqual(
    ["p0", "p1", "p2", "p3", "p4", "p1", "p6", "p7"].map((p) => new Player(p))
  );
  expect(gameBoard.isBoardFull()).toBe(true);
});

test("add and remove same player01", () => {
  const gameBoard = new GameBoard();
  const p1 = new Player("p1");
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.removePlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  gameBoard.addPlayer(p1);
  expect(gameBoard.getPlayers()).toStrictEqual([p1, p1, p1, p1, p1]);

  expect(gameBoard.isBoardFull()).toBe(false);
});

//TODO fix findWinner()
/*
test("findWinner", () => {
  const { gameBoard, players } = createGameBoardTestResources();
  const { winUnicorn } = createCardTestResources();
  winUnicorn.setUnicornValue(8);
  players.forEach((player) => gameBoard.addPlayer(player));
  winUnicorn.enterStable();
  expect(gameBoard.findWinner()).toBeInstanceOf(Player);
});

 */
