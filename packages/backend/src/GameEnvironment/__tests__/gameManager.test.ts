import { createGameBoardTestResources } from "../../Cards/__tests__/test-resources";
import { Player } from "../Player";

//TODO fix getActivePlayer() to not return undefined
/*
test("getActivePlayer returns Player", () => {
  const { gameBoard, gameManager, players } = createGameBoardTestResources();
  const { winUnicorn } = createCardTestResources();
  winUnicorn.setUnicornValue(8);
  players.forEach((player) => gameBoard.addPlayer(player));
  winUnicorn.enterStable();
  expect(gameManager.getActivePlayer()).toBeInstanceOf(Player);
});
*/

test("gameBoard.getPlayers returns players", () => {
  const { gameBoard, players } = createGameBoardTestResources();
  players.forEach((player) => gameBoard.addPlayer(player));
  expect(gameBoard.getPlayers()[0]).toBeInstanceOf(Player);
});

//TODO fix nextTurn()
/*
test("play several turns", () => {
  const { gameBoard, gameManager, players } = createGameBoardTestResources();
  players.forEach((player) => gameBoard.addPlayer(player));
  for (let i = 0; i < 10; i++) {
    gameManager.nextTurn();
  }
  expect(gameManager.getElapsedTurnCount()).toBe(9);
});
 */
