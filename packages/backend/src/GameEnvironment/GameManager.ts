import { GameBoard } from "./GameBoard";
import { EventType, GameBoardEvent } from "../Events/GameBoardEvent";
import { GameManagerReportTicket } from "./GameManagerReportTicket";
import { Player } from "./Player";
import { Stable } from "./Stable";
import { Card } from "../Cards/Card";

export class GameManager {
  private gameBoard: GameBoard;
  private elapsedTurnCount = -1;
  private events: Record<EventType, GameBoardEvent[]>;
  private activePlayer: Player | null = null;

  constructor(gameBoard: GameBoard) {
    this.gameBoard = gameBoard;

    this.events = Object.fromEntries(Object.entries(EventType).map(([, value]) => [value, []])) as unknown as Record<
      EventType,
      GameBoardEvent[]
    >;

    this.nextTurn();
  }

  private triggerEventsForCard(eventType: EventType, card: Card): void {
    const eventArray: GameBoardEvent[] = this.events[eventType].filter((e) => {
      return card === e.getPartOf();
    });

    eventArray.forEach((e) => {
      if (!(e.getType() === EventType.PLAY && e.getPartOf().getOwner() !== this.activePlayer)) {
        e.trigger();
      }
    });
  }

  public receiveReportTicket(reportTicket: GameManagerReportTicket): void {
    const card: Card = reportTicket.getSender();
    const eventType: EventType = reportTicket.getEventType();

    this.triggerEventsForCard(eventType, card);
  }

  public nextTurn(): void {
    const players: Player[] = this.gameBoard.getPlayers();
    const playerCount: number = players.length;

    while (this.activePlayer?.forcePlayerToDiscardCard()) {
      this.activePlayer?.forcePlayerToDiscardCard();
    }

    ++this.elapsedTurnCount;

    this.activePlayer = this.gameBoard.getPlayers()[this.elapsedTurnCount % playerCount];

    players.forEach((p) => {
      const stable: Stable = p.getStable();
      stable.getModifiers().forEach((m) => {
        this.triggerEventsForCard(EventType.TURN_START, m);
      });

      stable.getCreatures().forEach((c) => {
        this.triggerEventsForCard(EventType.TURN_START, c);
      });
    });
  }

  public addEvents(events: GameBoardEvent[]): void {
    events.forEach((e) => {
      this.events[e.getType()].push(e);
    });
  }

  public getActivePlayer(): Player | null {
    return this.activePlayer;
  }

  public getElapsedTurnCount(): number {
    return this.elapsedTurnCount;
  }
}
