import { Card } from "../Cards/Card";
import { GameBoard } from "./GameBoard";
import { GameManager } from "./GameManager";
import { CreatureType, CreatureSubType } from "../Cards/CreatureCard";
import { CreatureCard } from "../Cards/CreatureCard";
import { EventType } from "../Events/GameBoardEvent";
import { BasicCreaturePlayEvent } from "../Events/BasicCreaturePlayEvent";
import { BasicCreatureDestructionEvent } from "../Events/BasicCreatureDestructionEvent";
import { BasicReturnToHandEvent } from "../Events/BasicReturnToHandEvent";

export class Game {
  private id: string;
  private gameBoard: GameBoard = new GameBoard();
  private gameManager: GameManager = new GameManager(this.gameBoard);

  public constructor() {
    const cards = this.generateCards();

    cards.forEach((c) => {
      this.gameBoard.getDeck().addCard(c);
      this.gameManager.addEvents(c.getEvents());
    });

    this.gameBoard.getDeck().shuffle();
  }

  private generateCards(): Card[] {
    const ret = [];

    for (let i = 1; i <= 40; ++i) {
      const card = new CreatureCard(
        i.toString(),
        this.gameManager,
        "Narwhal",
        "",
        "",
        CreatureType.UNICORN,
        CreatureSubType.BASIC
      );

      const playEvent = new BasicCreaturePlayEvent(this.gameBoard, card);
      const destroyEvent = new BasicCreatureDestructionEvent(EventType.DESTROY, this.gameBoard, card);
      const discardEvent = new BasicCreatureDestructionEvent(EventType.DISCARD, this.gameBoard, card);
      const sacrificeEvent = new BasicCreatureDestructionEvent(EventType.SACRIFICE, this.gameBoard, card);
      const returnToHandEvent = new BasicReturnToHandEvent(this.gameBoard, card);

      card.addGameBoardEvent(playEvent);
      card.addGameBoardEvent(destroyEvent);
      card.addGameBoardEvent(discardEvent);
      card.addGameBoardEvent(sacrificeEvent);
      card.addGameBoardEvent(returnToHandEvent);

      ret.push(card);
    }

    return ret;
  }

  public getGameBoard(): GameBoard {
    return this.gameBoard;
  }

  public getGameManager(): GameManager {
    return this.gameManager;
  }
}
