import { Card } from "../Cards/Card";
import { EventType } from "../Events/GameBoardEvent";

export class GameManagerReportTicket {
  private sender: Card;
  private eventType: EventType;

  public constructor(sender: Card, eventType: EventType) {
    this.sender = sender;
    this.eventType = eventType;
  }

  public getSender(): Card {
    return this.sender;
  }

  public getEventType(): EventType {
    return this.eventType;
  }
}
