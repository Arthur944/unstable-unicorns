import { CardDeck } from "./CardDeck";
import { Player } from "./Player";

export class GameBoard {
  private players: Player[] = [];
  private playerLimit = 8;
  private unicornsToWin = 6;
  private nursery = new CardDeck();
  private deck = new CardDeck();
  private discardPile = new CardDeck();

  public getPlayers(): Player[] {
    return [...this.players];
  }

  public getNursery(): CardDeck {
    return this.nursery;
  }

  public getDeck(): CardDeck {
    return this.deck;
  }

  public getDiscardPile(): CardDeck {
    return this.discardPile;
  }

  public isBoardFull(): boolean {
    return this.players.length == this.playerLimit;
  }

  public addPlayer(player: Player): void {
    if (!this.isBoardFull()) {
      this.players.push(player);
    }
  }

  public removePlayer(player: Player): void {
    this.players = this.players.filter((elem) => !elem.isEqual(player));
  }

  public findWinner(): Player | null {
    return this.players.find((player) => player.getStable().countUnicorns() >= this.unicornsToWin) || null;
  }
}
