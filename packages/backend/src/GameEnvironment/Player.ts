import { Card } from "../Cards/Card";
import { Stable } from "./Stable";

export class Player {
  private name: string;
  private handLimit = 7;
  private hand: Card[] = [];
  private actionPoints = 1;
  private stable: Stable = new Stable(this);

  constructor(name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }

  public getActionPoints(): number {
    return this.actionPoints;
  }

  public getHand(): Card[] {
    return [...this.hand];
  }

  public getStable(): Stable {
    return this.stable;
  }

  public setActionPoints(value: number) {
    this.actionPoints = value;
  }

  public addCardToHand(card: Card) {
    this.hand.push(card);
    card.setOwner(this);
  }

  public removeCardFromHand(card: Card) {
    const index = this.hand.indexOf(card, 0);

    if (index > -1) {
      this.hand.splice(index, 1);
      card.setOwner(null);
    }
  }

  public isHandEmpty(): boolean {
    return this.hand.length == 0;
  }

  public isHandFull(): boolean {
    return this.hand.length < this.handLimit;
  }

  public playCardFromHand(index: number) {
    if (!this.isHandEmpty() && index >= 0 && index < this.hand.length) {
      const card: Card = this.hand[index];
      card.play();
    } else {
      throw new RangeError();
    }
  }

  public isEqual(other: Player) {
    return other.name === this.name;
  }

  public forcePlayerToDiscardCard(): boolean {
    if (this.hand.length + 1 < this.handLimit) {
      //call GameBoardEvent
      return true;
    }
    return false;
  }
}
