import { Card } from "../Cards/Card";
import { shuffle } from "lodash-es";

export class CardDeck {
  private cards: Card[];

  public constructor(cards?: Card[]) {
    this.cards = [];

    if (cards) {
      cards.forEach((c) => this.addCard(c));
    }
  }

  public getCards(): Card[] {
    return [...this.cards];
  }

  public addCard(card: Card): void {
    this.cards.push(card);
    card.setOwner(null);
  }

  public isEmpty(): boolean {
    return this.cards.length == 0;
  }

  public draw(): Card | null {
    return !this.isEmpty() ? this.cards.pop() || null : null;
  }

  public shuffle(): void {
    this.cards = shuffle([...this.cards]);
  }
}
