import { registerAuth } from "@litbase/server";
import { server } from "./litbase-server";
import { database, registerModels } from "./database";
import { myAppNamespace } from "@unstable-unicorns/common/controllers/controller-interface";
import { Controller } from "./controller";
import { Collections } from "@unstable-unicorns/common/models/collections";

registerAuth(server, {
  redisUrl: process.env.REDIS_URL || "",
  sessionSecret: process.env.SESSION_SECRET as string,
  usersCollection: database.getCollection("users"),
  providers: {
    email: {
      fromAddress: process.env.EMAIL_FROM_ADDRESS as string,
      mailgun: {
        apiKey: process.env.MAILGUN_API_KEY as string,
        domain: process.env.MAILGUN_DOMAIN as string,
        apiDomain: process.env.MAILGUN_API_DOMAIN as string,
        validationApiKey: process.env.MAILGUN_VALIDATION_API_KEY,
      },
      captchaSecret: process.env.RECAPTCHA_SECRET as string,
    },
  },
});

server.registerClass(myAppNamespace, new Controller());

registerModels(database, server.litql);

(async function () {
  const litbasePort = Number(process.env.PORT || process.env.SERVER_PORT || "8002");

  try {
    await server.startListening(litbasePort, process.env.SERVER_LISTEN_ADDRESS || undefined);
    console.debug("Litbase server is listening on ", litbasePort);
    setTimeout(() => {
      server.litql.queryTypeOnce(Collections.CARDS, {
        $matchAll: {
          QualityImg: { $exists: true },
        },
        $pull: 1,
      });
    }, 1500);
  } catch (error) {
    console.error(error);
  }
})();
