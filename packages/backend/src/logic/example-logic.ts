export const a = 5;

export function sayHello() {
  return "Hello";
}

export function add(a: number, b: number) {
  return a + b;
}
