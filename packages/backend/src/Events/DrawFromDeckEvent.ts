import { EventType } from "./GameBoardEvent";
import { Card } from "../Cards/Card";
import { GameBoard } from "../GameEnvironment/GameBoard";
import { CardRestriction } from "../Cards/CardRestriction";
import { DrawEvent } from "./DrawEvent";

export class DrawFromDeckEvent extends DrawEvent {
  public constructor(
    type: EventType,
    gameBoard: GameBoard,
    partOf: Card,
    optional: boolean,
    restriction: CardRestriction,
    drawAmount: number
  ) {
    // @ts-ignore
    super(type, gameBoard, partOf, optional, restriction, drawAmount);
    this.deck = this.gameBoard.getDeck();
  }
}
