import { GameBoardEvent, EventType } from "./GameBoardEvent";
import { Card } from "../Cards/Card";
import { GameBoard } from "../GameEnvironment/GameBoard";
import { Stable } from "../GameEnvironment/Stable";
import { CreatureCard } from "../Cards/CreatureCard";
import { Player } from "../GameEnvironment/Player";

export class BasicCreatureDestructionEvent extends GameBoardEvent {
  public constructor(type: EventType, gameBoard: GameBoard, partOf: Card) {
    super(type, gameBoard, partOf, false, undefined);
  }

  public trigger(): void {
    const owner: Player | null = this.partOf.getOwner();
    if (!owner) throw new Error();
    const stable: Stable = owner.getStable();
    stable.removeCard(this.partOf as CreatureCard);
    this.gameBoard.getDiscardPile().addCard(this.partOf);
  }
}
