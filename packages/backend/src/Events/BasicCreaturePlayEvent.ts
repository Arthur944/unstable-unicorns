import { GameBoardEvent, EventType } from "./GameBoardEvent";
import { Card } from "../Cards/Card";
import { GameBoard } from "../GameEnvironment/GameBoard";
import { Player } from "../GameEnvironment/Player";
import { Stable } from "../GameEnvironment/Stable";
import { CreatureCard } from "../Cards/CreatureCard";

export class BasicCreaturePlayEvent extends GameBoardEvent {
  public constructor(gameBoard: GameBoard, partOf: Card) {
    super(EventType.PLAY, gameBoard, partOf, false, undefined);
  }

  public trigger(): void {
    const owner: Player | null = this.partOf.getOwner();
    if (!owner) throw new Error();
    const stable: Stable = owner.getStable();

    owner.removeCardFromHand(this.partOf);
    stable.addCard(this.partOf as CreatureCard);
  }
}
