import { Card } from "../Cards/Card";
import { Player } from "../GameEnvironment/Player";
import { CardRestriction } from "../Cards/CardRestriction";
import { GameBoard } from "../GameEnvironment/GameBoard";

export enum EventType {
  PLAY = "play",
  DISCARD = "discard",
  TURN_START = "turn-start",
  ENTER_STABLE = "enter-stable",
  LEAVE_STABLE = "leave-stable",
  SACRIFICE = "sacrifice",
  DESTROY = "destroy",
  RETURN_TO_HAND = "return-to-hand",
}

export abstract class GameBoardEvent {
  private type: EventType;
  private alternative: GameBoardEvent | null;
  private optional: boolean;
  protected gameBoard: GameBoard;
  protected targets: Player[];
  protected partOf: Card;
  protected cardRestriction?: CardRestriction;

  protected constructor(
    type: EventType,
    gameBoard: GameBoard,
    partOf: Card,
    optional: boolean,
    restriction?: CardRestriction
  ) {
    this.type = type;
    this.gameBoard = gameBoard;
    this.targets = [];
    this.partOf = partOf;
    this.alternative = null;
    this.optional = optional;
    this.cardRestriction = restriction;
  }

  public getType(): EventType {
    return this.type;
  }

  public getPartOf(): Card {
    return this.partOf;
  }

  public getAlternative(): GameBoardEvent | null {
    return this.alternative;
  }

  public isOptional(): boolean {
    return this.optional;
  }

  public getCardRestriction(): CardRestriction | undefined {
    return this.cardRestriction;
  }

  public setTargets(targets: Player[]): void {
    this.targets = [];

    targets.forEach((p) => {
      this.targets.push(p);
    });
  }

  public setAlternative(event: GameBoardEvent): void {
    this.alternative = event;
  }

  public abstract trigger(): void;
}
