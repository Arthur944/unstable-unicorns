import { GameBoardEvent, EventType } from "./GameBoardEvent";
import { Card } from "../Cards/Card";
import { GameBoard } from "../GameEnvironment/GameBoard";
import { Player } from "../GameEnvironment/Player";

export class BasicDiscardEvent extends GameBoardEvent {
  public constructor(gameBoard: GameBoard, partOf: Card) {
    super(EventType.DISCARD, gameBoard, partOf, false, undefined);
  }

  public trigger(): void {
    const owner: Player | null = this.partOf.getOwner();
    if (!owner) throw new Error();

    owner.removeCardFromHand(this.partOf);
    this.gameBoard.getDiscardPile().addCard(this.partOf);
  }
}
