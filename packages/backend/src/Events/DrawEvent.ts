import { EventType, GameBoardEvent } from "./GameBoardEvent";
import { Card } from "../Cards/Card";
import { GameBoard } from "../GameEnvironment/GameBoard";
import { CardRestriction } from "../Cards/CardRestriction";
import { CardDeck } from "../GameEnvironment/CardDeck";

export abstract class DrawEvent extends GameBoardEvent {
  private drawAmount: number;
  protected deck: CardDeck | undefined;

  protected constructor(
    type: EventType,
    gameBoard: GameBoard,
    partOf: Card,
    optional: boolean,
    restriction: CardRestriction,
    drawAmount: number,
    deck: CardDeck
  ) {
    super(type, gameBoard, partOf, optional, restriction);
    this.drawAmount = drawAmount;
    this.deck = deck;
  }

  public trigger(): void {
    this.targets.forEach((player) => {
      for (let i = 0; i < this.drawAmount; ++i) {
        const card = this.deck?.draw();
        if (!card) throw new Error();
        player.addCardToHand(card);
      }
    });
  }
}
