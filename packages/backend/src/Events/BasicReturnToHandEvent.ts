import { GameBoardEvent, EventType } from "./GameBoardEvent";
import { Card, CardType } from "../Cards/Card";
import { GameBoard } from "../GameEnvironment/GameBoard";
import { Player } from "../GameEnvironment/Player";
import { Stable } from "../GameEnvironment/Stable";
import { CreatureCard } from "../Cards/CreatureCard";
import { ModifierCard } from "../Cards/ModifierCard";

export class BasicReturnToHandEvent extends GameBoardEvent {
  public constructor(gameBoard: GameBoard, partOf: Card) {
    super(EventType.RETURN_TO_HAND, gameBoard, partOf, false, undefined);
  }

  public trigger(): void {
    const owner: Player | null = this.partOf.getOwner();
    if (!owner) throw new Error();

    const stable: Stable = owner.getStable();

    if (this.partOf.getType() === CardType.CREATURE) {
      stable.removeCard(this.partOf as CreatureCard);
    } else if (this.partOf.getType() === CardType.MODIFIER) {
      stable.removeCard(this.partOf as ModifierCard);
    } else throw "CardType is not correct";

    owner.addCardToHand(this.partOf);
  }
}
