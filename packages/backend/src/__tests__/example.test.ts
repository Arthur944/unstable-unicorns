import { expect } from "chai";
import { add } from "../logic/example-logic";

describe("Number adder", () => {
  test("Should add 2 numbers together", () => {
    expect(add(2, 2)).to.equal(4);
    expect(add(2, -2)).to.equal(0);
  });
});
