/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

const projectName = "@unstable-unicorns";

module.exports = {
  testMatch: ["**/?(*.)+(spec|test).[jt]s?(x)"],

  globals: {
    __TESTING__: true,
    REDIS_URL:
      "redis://default:fVu7vxkqo4g8d55WBqQLkJVJULefrZhv@redis-14764.c291.ap-southeast-2-1.ec2.cloud.redislabs.com:14764",
  },

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",

  // The maximum amount of workers used to run your tests. Can be specified as % or a number. E.g. maxWorkers: 10% will use 10% of your CPU amount + 1 as the maximum worker number. maxWorkers: 2 will use a maximum of 2 workers.
  maxWorkers: "75%",

  // An array of directory names to be searched recursively up from the requiring module's location
  moduleDirectories: ["node_modules"],

  // An array of file extensions your modules use
  moduleFileExtensions: ["js", "json", "jsx", "ts", "tsx", "node"],

  // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
  moduleNameMapper: {
    "^lodash-es$": "lodash",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "identity-obj-proxy",
    "\\.(css|less)$": "<rootDir>/__mocks__/styleMock.js",
    [`^${projectName}(\\|/)common(.*)$`]: "<rootDir>/../common/src$1",
  },

  // A map from regular expressions to paths to transformers
  transform: {
    "^.+\\.(js|jsx|tsx|ts)$": "babel-jest",
    ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2|srt|csv|txt|jar)$": "jest-transform-stub",
  },

  // An array of regexp pattern strings that are matched against all source file paths, matched files will skip transformation
  transformIgnorePatterns: ["/node_modules/(?!(lodash-es|@litbase|msgpackr))"],

  setupFilesAfterEnv: ["jest-extended", "dotenv/config"],
};
