module.exports = function (api) {
  api.cache(true);

  return {
    presets: [
      [
        "@babel/preset-env",
        {
          targets: {
            node: true,
          },
          loose: true,
        },
      ],
      ["@babel/preset-typescript", { isTSX: true, allExtensions: true }],
    ],
    plugins: [
      ["@babel/plugin-proposal-decorators", { /*decoratorsBeforeExport: true,*/ legacy: true }],
      ["@babel/plugin-proposal-class-properties", { loose: true }],
      ["@babel/plugin-proposal-optional-chaining", { loose: true }],
      ["@babel/plugin-proposal-nullish-coalescing-operator", { loose: true }],
    ],
  };
};
