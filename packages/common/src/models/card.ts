import { createBinaryId, DocumentInterface } from "@litbase/core";
import { Collections } from "@unstable-unicorns/common/models/collections";

export enum CardType {
  BABY = "baby",
}

export interface Card extends DocumentInterface {
  name: string;
  type: CardType;
  image: string;
  description: string;
}

export function createEmptyCard(): Omit<Card, "type"> {
  return {
    _id: createBinaryId(),
    _collection: Collections.CARDS,
    name: "",
    image: "",
    description: "",
  };
}
