export enum Collections {
  USERS = "users",
  TEST_DOCS = "test-docs",
  CARDS = "cards",
  GAMES = "games",
  HANDS = "hands",
  DISCARD_PILES = "discard-piles",
  DRAW_DECKS = "draw-decks",
  STABLES = "stables",
}
