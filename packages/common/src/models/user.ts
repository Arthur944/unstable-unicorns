import { BaseUserInterface } from "@litbase/core";

export interface User extends BaseUserInterface {
  exampleAttribute: string;
}
