import { BinaryId, DocumentInterface } from "@litbase/core";
import { Card } from "@unstable-unicorns/common/models/card";

export interface DiscardPile extends DocumentInterface {
  gameId: BinaryId;
  cardIds: BinaryId[];
  cards?: Card[];
}
