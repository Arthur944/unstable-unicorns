import { BinaryId, DocumentInterface } from "@litbase/core";
import { Card } from "@unstable-unicorns/common/models/card";

export interface Hand extends DocumentInterface {
  userId: BinaryId;
  gameId: BinaryId;
  cardIds: BinaryId[];
  cards: Card[];
}
