import { BinaryId, DocumentInterface } from "@litbase/core";

export interface Stable extends DocumentInterface {
  gameId: BinaryId;
  playerId: BinaryId;
  cardIds: BinaryId[];
}
