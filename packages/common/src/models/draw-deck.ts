import { BinaryId, createBinaryId, DocumentInterface } from "@litbase/core";
import { Collections } from "@unstable-unicorns/common/models/collections";

export interface DrawDeck extends DocumentInterface {
  gameId: BinaryId;
  cards: { cardId: BinaryId; inDeck: boolean }[];
}

export function createEmptyDrawDeck(): Omit<DrawDeck, "gameId"> {
  return {
    _id: createBinaryId(),
    _collection: Collections.DRAW_DECKS,
    cards: [],
  };
}
