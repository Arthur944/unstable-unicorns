import { BinaryId, DocumentInterface } from "@litbase/core";
import { User } from "@unstable-unicorns/common/models/user";

export interface Game extends DocumentInterface {
  creatorId: BinaryId;
  userIds: BinaryId[];
  startedAt?: Date;
  activePlayerIndex?: number;
  creator?: User;
  users?: User[];
}
