export const myAppNamespace = "my-app";
export interface ControllerInterface {
  doSomething(args: { testArg: string }): Promise<string>;
}

export const myAppPromiseFunctions = ["doSomething"];
