import { BrowserRouter, Route } from "react-router-dom";
import { WelcomePage } from "./pages/welcome/welcome-page";
import { baseGlobalStyle, ModalProvider } from "@litbase/alexandria";
import { LitbaseContext } from "@litbase/react";
import { litbaseClient } from "../litbase-client";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { theme } from "../theme/themes";
import { GamePage } from "./pages/game-page";
import { Paths } from "../utils/paths";
import { isEmptyUser, isUserGuest } from "@litbase/core";
import { GameBrowser } from "./pages/game-browser/game-browser";
import { useUser } from "../hooks/use-user";
import "tippy.js/dist/tippy.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";

export function App() {
  const user = useUser();
  const isLoggedIn = !isEmptyUser(user) && !isUserGuest(user);
  return (
    // @ts-ignore
    <LitbaseContext.Provider value={litbaseClient}>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <BrowserRouter>
          <ModalProvider>
            {!isLoggedIn && (
              <>
                {/* @ts-ignore */}
                <Route path={Paths.MAIN} component={WelcomePage} exact />
              </>
            )}
            {isLoggedIn && (
              <>
                {/* @ts-ignore */}
                <Route path={Paths.MAIN} component={GameBrowser} exact />
                {/* @ts-ignore */}
                <Route path={Paths.GAME + "/:id"} component={GamePage} exact />
              </>
            )}
          </ModalProvider>
          <ToastContainer />
        </BrowserRouter>
      </ThemeProvider>
    </LitbaseContext.Provider>
  );
}

const GlobalStyle = createGlobalStyle`
  input {
    font-weight: bold;
  }
  body {
    font-weight: bold;
  }
  ${baseGlobalStyle};
`;
