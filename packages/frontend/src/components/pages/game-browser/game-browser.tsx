import styled from "styled-components";
import bgSrc from "./images/bg.png";
import { Card as CardBody, spacing12, spacing2, spacing24, spacing3, spacing48, spacing8 } from "@litbase/alexandria";
import { useQueryType } from "@litbase/react/hooks/use-query";
import { Collections } from "@unstable-unicorns/common/models/collections";
import babyUnicorn from "./images/baby-unicorn.png";
import GroupIcon from "./images/group-icon.svg";
import flyingUnicorn from "./images/flying-unicorn.png";
import { Plus } from "@styled-icons/boxicons-regular";
import { Game } from "@unstable-unicorns/common/models/game";
import { useUser } from "../../../hooks/use-user";
import { useHistory } from "react-router-dom";
import { Paths } from "../../../utils/paths";
import { createGame, deleteGame } from "../../game/actions/create-game";
import { joinGame } from "../../game/actions/join-game";

export function GameBrowser() {
  const user = useUser();
  const [games] = useQueryType<Game>(Collections.GAMES, {
    _id: 1,
    name: 1,
    userIds: 1,
    creator: {
      name: 1,
      _id: 1,
    },
  });
  return (
    <Body>
      <MainCard>
        <FlavorImage src={flyingUnicorn} />
        <Title>Find game</Title>
        <GameList>
          {games.map((elem) => (
            <GameListItem key={elem._id.value} game={elem} />
          ))}
          {games.length === 0 && <EmptyMessage>There are no running games</EmptyMessage>}
          <GameBody>
            <span />
            <RoomName onClick={() => createGame(user)}>
              <Plus />
              Create game
            </RoomName>
            <span />
          </GameBody>
        </GameList>
      </MainCard>
    </Body>
  );
}

function GameListItem({ game }: { game: Game }) {
  const history = useHistory();
  const user = useUser();
  return (
    <GameBody
      onClick={async () => {
        await joinGame(game, user);
        history.push(Paths.GAME + "/" + game._id.toUrlSafeString());
      }}
    >
      <BabyUnicorn src={babyUnicorn} />
      <RoomName>{game.creator?.name}'s Room</RoomName>
      <IconContainer>
        <GroupIcon /> {game.userIds.length}
        {game.creatorId.equals(user._id) && (
          <button
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              deleteGame(game._id);
            }}
          >
            delete
          </button>
        )}
      </IconContainer>
    </GameBody>
  );
}

const EmptyMessage = styled.span`
  font-size: ${({ theme }) => theme.text3xl};
  color: ${({ theme }) => theme.gray300};
  margin: 0 auto;
`;

const FlavorImage = styled.img`
  position: absolute;
  top: -${spacing24};
  right: -${spacing24};
  width: ${spacing48};
`;

const Title = styled.span`
  font-size: ${({ theme }) => theme.text2xl};
`;

const IconContainer = styled.div`
  font-size: ${({ theme }) => theme.text3xl};
`;

const RoomName = styled.span`
  font-size: ${({ theme }) => theme.text3xl};
`;

const BabyUnicorn = styled.img`
  width: ${spacing12};
  height: ${spacing12};
  border-radius: ${({ theme }) => theme.roundedFull};
`;

const GameList = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: ${spacing8};
  > * + * {
    margin-top: ${spacing3};
  }
`;

const GameBody = styled.div`
  display: flex;
  width: 100%;
  border: 3px solid ${({ theme }) => theme.gray200};
  border-radius: ${({ theme }) => theme.roundedXl};
  padding: ${spacing2};
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  transition: background-color 0.3s;
  color: black;
  &:hover {
    text-decoration: none;
    background: ${({ theme }) => theme.gray100};
  }
`;

const MainCard = styled(CardBody)`
  position: relative;
  font-weight: bold;
  width: 100%;
  max-width: 1024px;
`;

const Body = styled.div`
  height: 100vh;
  width: 100%;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url(${bgSrc});
  background-size: cover;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: ${spacing8};
`;
