import { spacing3, spacing4, spacing8 } from "@litbase/alexandria";
import styled from "styled-components";
import bgSrc from "./images/background image.png";
import smudgeSrc from "./images/smudge.png";
import logoSrc from "./images/logo.png";
import { Input } from "../../inputs/input";
import { Button } from "../../inputs/button";
import { ReactNode, useMemo } from "react";
import { litbaseClient } from "../../../litbase-client";
import { Field, Formik, useFormikContext } from "formik";
import { Link, useLocation } from "react-router-dom";

export function WelcomePage() {
  const location = useLocation();
  const isLogin = useMemo(() => {
    const params = new URLSearchParams(location.search.slice(1));
    return !!params.get("login");
  }, [location.search]);
  return (
    <Page>
      <Col>
        <Formik
          initialValues={{ email: "", name: "", password: "" }}
          onSubmit={(values) => handleSubmit(values, isLogin)}
        >
          <>
            <Title>
              <Logo src={logoSrc} alt={"Unstalbe unicorns"} />
            </Title>
            <Field placeholder={"Email"} name={"email"} component={StyledInput} />
            {!isLogin && <Field placeholder={"Username"} component={StyledInput} name={"name"} />}
            <Field placeholder={"Password"} component={StyledInput} name={"password"} type={"password"} />
            <SubmitButton>{isLogin ? "Login" : "Register"}</SubmitButton>
            <SwitchLink to={"/" + isLogin ? "?login=true" : ""}>
              {!isLogin ? "Login instead" : "Register instead"}
            </SwitchLink>
          </>
        </Formik>
      </Col>
    </Page>
  );
}

function SubmitButton({ children }: { children: ReactNode }) {
  const { submitForm } = useFormikContext();
  return <StyledButton onClick={() => submitForm()}>{children}</StyledButton>;
}

async function handleSubmit(
  { name, email, password }: { name: string; email: string; password: string },
  isLogin: boolean
) {
  if (!isLogin) {
    await litbaseClient.registerWithEmailAndPassword({ email, name, password });
  }
  await litbaseClient.loginWithEmailAndPassword({ email, password });
}

const SwitchLink = styled(Link)`
  margin-bottom: auto;
  color: black;
  font-size: ${({ theme }) => theme.text2xl};
  margin-top: ${spacing4};
`;

const StyledButton = styled(Button)`
  margin-top: ${spacing8};
`;

const StyledInput = styled(Input)`
  width: 75%;
  margin-top: ${spacing3};
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: fit-content;
  margin: 0 auto;
  align-items: center;
`;

const Logo = styled.img`
  margin-left: ${spacing8};
`;

const Title = styled.div`
  background: url("${smudgeSrc}");
  width: 50rem;
  height: 27rem;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: auto;
`;

const Page = styled.div`
  padding: ${spacing4};
  background: url("${bgSrc}");
  height: 100vh;
  background-size: cover;
  background-repeat: no-repeat;
`;
