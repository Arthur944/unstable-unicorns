import styled from "styled-components";
import grassTile from "../../assets/images/grass.jpg";
import { MyStable } from "../game/stables/my-stable";
import { EnemyStables } from "../game/stables/enemy-stables";
import { DrawDeckComponent } from "../game/draw-deck";
import { DiscardDeck } from "../game/discard-deck";
import { CardPlayingArea } from "../game/card-playing-area";
import { useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { RouteComponentProps } from "react-router-dom";
import { BinaryId } from "@litbase/core";
import { Game } from "@unstable-unicorns/common/models/game";
import { GameContext } from "../game/game-context";
import { Starter } from "../game/starter";
import { ActionReminder } from "../game/action-reminder";
import { ActionButtons } from "../game/action-buttons";

export function GamePage({
  match: {
    params: { id },
  },
}: RouteComponentProps<{ id: string }>) {
  const gameId = BinaryId.fromUrlSafeString(id);
  const [game] = useQueryTypeOne<Game>(Collections.GAMES, { $match: { _id: gameId } });

  return (
    <GameContext.Provider value={{ gameId, ...game, userIds: game?.userIds || [] }}>
      <Body>
        <Starter />
        <ActionReminder />
        <ActionButtons />
        <EnemyStables>
          <MainArea>
            <MainRow>
              <DiscardDeck />
              <CardPlayingArea />
              <DrawDeckComponent />
            </MainRow>
            <MyStable />
          </MainArea>
        </EnemyStables>
      </Body>
    </GameContext.Provider>
  );
}

const MainRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const MainArea = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 50% 50%;
`;

const Body = styled.div`
  width: 100%;
  height: 100vh;
  background: url("${grassTile}");
`;
