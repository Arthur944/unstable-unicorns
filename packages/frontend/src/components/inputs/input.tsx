import styled from "styled-components";
import { ComponentProps, forwardRef } from "react";

export const Input = forwardRef<HTMLInputElement>(
  (props: ComponentProps<"input"> & { field?: Record<string, unknown> }, ref) => {
    return <Body {...props.field} {...props} ref={ref} />;
  }
);

const Body = styled.input`
  border-radius: ${({ theme }) => theme.roundedLg};
  font-size: ${({ theme }) => theme.text3xl};
  border: none;
  text-align: center;
`;
