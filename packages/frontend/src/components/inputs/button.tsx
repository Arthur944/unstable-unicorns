import styled from "styled-components";
import { ComponentProps, forwardRef, ReactNode } from "react";
import { spacing2, spacing8 } from "@litbase/alexandria";

export const Button = forwardRef<HTMLButtonElement, ComponentProps<"button"> & { children?: ReactNode }>(
  ({ className, ...props }, ref) => {
    return (
      <BodyWrapper className={className}>
        <Body {...props} ref={ref}>
          {props.children}
        </Body>
      </BodyWrapper>
    );
  }
);

const BodyWrapper = styled.div`
  background: white;
  border-radius: 999rem;
`;

const Body = styled.button`
  font-size: ${({ theme }) => theme.text4xl};
  border-radius: 999rem;
  width: fit-content;
  background: linear-gradient(90deg, #ed2524 -3.33%, #fec919 18.73%, #6ec379 47.8%, #508cca 73.09%, #a470b0 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  text-transform: uppercase;
  font-weight: bold;
  padding: ${spacing2} ${spacing8};
  border: none;
`;
