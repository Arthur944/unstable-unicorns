import { useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { useGame } from "./game-context";
import { Game } from "@unstable-unicorns/common/models/game";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { useUser } from "../../hooks/use-user";
import { useEffect } from "react";
import { toast } from "react-toastify";

export function ActionReminder() {
  const { gameId } = useGame();
  const [game] = useQueryTypeOne<Game>(Collections.GAMES, { $match: { _id: gameId } });
  const user = useUser();

  useEffect(() => {
    if (game?.activePlayerIndex === undefined || !game?.userIds[game?.activePlayerIndex || 0].equals(user._id)) {
      return;
    } else {
      toast(<>Draw a card!</>);
    }
  }, [game?.activePlayerIndex, user._id, true]);
  return null;
}
