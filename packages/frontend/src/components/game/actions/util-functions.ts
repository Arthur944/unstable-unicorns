import { DrawDeck } from "@unstable-unicorns/common/models/draw-deck";
import { BinaryId } from "@litbase/core";
import { sampleSize } from "lodash-es";

export function getRandom5CardsFromDrawDeck(deck: Pick<DrawDeck, "cards">): BinaryId[] {
  const cardIds = deck.cards.filter((elem) => elem.inDeck).map((elem) => elem.cardId);
  return sampleSize<BinaryId>(cardIds, 5);
}
