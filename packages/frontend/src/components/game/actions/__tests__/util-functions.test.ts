import { createBinaryId } from "@litbase/core";
import { expect } from "chai";
import { getRandom5CardsFromDrawDeck } from "../util-functions";

describe("Util functions", () => {
  test("Should draw 5 different random available cards from the given draw deck", () => {
    // The first 5 cards are in the deck, the last five are not.
    const cards = [
      ...new Array(5).fill(0).map(() => ({
        cardId: createBinaryId(),
        inDeck: true,
      })),
      ...new Array(5).fill(0).map(() => ({
        cardId: createBinaryId(),
        inDeck: false,
      })),
    ];

    // Run it 100 times to make sure that we weren't just lucky with the random draw
    for (let i = 0; i < 100; i++) {
      const drawnCardIds = getRandom5CardsFromDrawDeck({ cards });
      const cardIds = drawnCardIds.map((elem) => elem.value);
      expect(new Set(cardIds).size).to.be.eq(5); // Make sure there are 5 **different** cards
      expect(cardIds).to.members(cards.slice(0, 5).map((elem) => elem.cardId.value));
      expect(cardIds).to.not.members(cards.slice(5).map((elem) => elem.cardId.value));
    }
  });
});
