import { litbaseClient } from "../../../litbase-client";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { Hand } from "@unstable-unicorns/common/models/hand";
import { BinaryId } from "@litbase/core";
import { DiscardPile } from "@unstable-unicorns/common/models/discard-pile";
import { Card } from "@unstable-unicorns/common/models/card";
import { Game } from "@unstable-unicorns/common/models/game";

export async function discardCard({
  discardPile,
  card,
  userId,
  game,
}: {
  discardPile: DiscardPile;
  card: Card;
  userId: BinaryId;
  game: Game;
}) {
  await Promise.all([
    litbaseClient.litql.queryTypeOnce<DiscardPile>(Collections.DISCARD_PILES, {
      $match: { _id: discardPile._id },
      $update: {
        $push: {
          cardIds: card._id,
        },
      },
    }),
    litbaseClient.litql.queryTypeOnce<Hand>(Collections.HANDS, {
      $match: {
        userId: userId,
        gameId: game._id,
      },
      $update: {
        $pull: {
          cardIds: card._id,
        },
      },
    }),
    litbaseClient.litql.queryTypeOnce<Game>(Collections.GAMES, {
      $match: {
        _id: game._id,
      },
      $update: {
        $set: {
          activePlayerIndex: ((game.activePlayerIndex || 0) + 1) % game.userIds.length,
        },
      },
    }),
  ]);
}
