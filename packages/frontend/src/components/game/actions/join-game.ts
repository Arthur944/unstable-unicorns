import { Game } from "@unstable-unicorns/common/models/game";
import { User } from "@unstable-unicorns/common/models/user";
import { litbaseClient } from "../../../litbase-client";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { DrawDeck } from "@unstable-unicorns/common/models/draw-deck";
import { Hand } from "@unstable-unicorns/common/models/hand";
import { Stable } from "@unstable-unicorns/common/models/stable";
import { Card, CardType } from "@unstable-unicorns/common/models/card";
import { createBinaryId } from "@litbase/core";
import { sample } from "lodash-es";
import { getRandom5CardsFromDrawDeck } from "./util-functions";

export async function joinGame(game: Game, user: User) {
  const alreadyExistingGame = await litbaseClient.litql.queryTypeOneOnce<Game>(Collections.GAMES, {
    $match: { _id: game._id },
  });
  if (alreadyExistingGame?.userIds.some((elem) => user._id.equals(elem))) {
    // Since the user already joined, we don't need to do anything further.
    return;
  }
  const drawDeck = await litbaseClient.litql.queryTypeOneOnce<DrawDeck>(Collections.DRAW_DECKS, {
    $match: { gameId: game._id },
  });
  if (!drawDeck) {
    throw new Error("Draw deck doesn't exist!");
  }
  const hand = getRandom5CardsFromDrawDeck(drawDeck);
  await litbaseClient.litql.queryTypeOnce<Hand>(Collections.HANDS, {
    $push: {
      userId: user._id,
      gameId: game._id,
      cardIds: hand,
    } as Hand,
  });
  await litbaseClient.litql.queryTypeOnce(Collections.DRAW_DECKS, {
    $match: { _id: drawDeck._id },
    $update: {
      $set: {
        cards: drawDeck.cards.map((elem) => ({
          ...elem,
          inDeck: !elem.inDeck ? elem.inDeck : !hand.some((handId) => handId.equals(elem.cardId)),
        })),
      },
    },
  });
  const babyCard = await getBabyUnicornCardForNewUser(game);
  await litbaseClient.litql.queryTypeOnce(Collections.STABLES, {
    $push: {
      _id: createBinaryId(),
      _collection: Collections.STABLES,
      gameId: game._id,
      playerId: user._id,
      cardIds: [babyCard._id],
    } as Stable,
  });
  await litbaseClient.litql.queryTypeOnce(Collections.GAMES, {
    $match: { _id: game._id },
    $update: {
      $push: {
        userIds: user._id,
      },
    },
  });
}

async function getBabyUnicornCardForNewUser(game: Game): Promise<Card> {
  const existingStables = await litbaseClient.litql.queryTypeOnce<Stable>(Collections.STABLES, {
    $matchAll: {
      gameId: game._id,
    },
  });
  const babyCards = await litbaseClient.litql.queryTypeOnce<Card>(Collections.CARDS, {
    $matchAll: {
      type: CardType.BABY,
    },
  });
  const takenIds = existingStables
    .map((elem) => elem.cardIds)
    .flat()
    .filter((elem) => babyCards.some((babyCard) => babyCard._id.equals(elem)));
  return sample<Card>(babyCards.filter((elem) => !takenIds.some((takenId) => elem._id.equals(takenId)))) as Card;
}
