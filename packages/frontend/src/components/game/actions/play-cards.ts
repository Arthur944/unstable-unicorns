import { litbaseClient } from "../../../litbase-client";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { Hand } from "@unstable-unicorns/common/models/hand";
import { BinaryId } from "@litbase/core";
import { Stable } from "@unstable-unicorns/common/models/stable";
import { Card } from "@unstable-unicorns/common/models/card";
import { Game } from "@unstable-unicorns/common/models/game";

export async function playCard({
  stable,
  card,
  userId,
  game,
}: {
  stable: Stable;
  card: Card;
  userId: BinaryId;
  game: Game;
}) {
  await Promise.all([
    litbaseClient.litql.queryTypeOnce<Stable>(Collections.STABLES, {
      $match: { _id: stable._id },
      $update: {
        $push: {
          cardIds: card._id,
        },
      },
    }),
    litbaseClient.litql.queryTypeOnce<Hand>(Collections.HANDS, {
      $match: {
        userId: userId,
        gameId: game._id,
      },
      $update: {
        $pull: {
          cardIds: card._id,
        },
      },
    }),
    litbaseClient.litql.queryTypeOnce<Game>(Collections.GAMES, {
      $match: {
        _id: game._id,
      },
      $update: {
        $set: {
          activePlayerIndex: ((game.activePlayerIndex || 0) + 1) % game.userIds.length,
        },
      },
    }),
  ]);
}
