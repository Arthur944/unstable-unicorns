import { User } from "@unstable-unicorns/common/models/user";
import { litbaseClient } from "../../../litbase-client";
import { Card, CardType } from "@unstable-unicorns/common/models/card";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { DiscardPile } from "@unstable-unicorns/common/models/discard-pile";
import { DrawDeck } from "@unstable-unicorns/common/models/draw-deck";
import { BinaryId, createBinaryId } from "@litbase/core";

export async function createGame(user: User) {
  const gameId = createBinaryId();
  const cards = await litbaseClient.litql.queryTypeOnce<Card>(Collections.CARDS, {});
  await litbaseClient.litql.queryTypeOnce(Collections.GAMES, {
    $push: {
      _id: gameId,
      _collection: Collections.GAMES,
      creatorId: user._id,
      userIds: [],
    },
  });
  await litbaseClient.litql.queryTypeOnce(Collections.DISCARD_PILES, {
    $push: {
      gameId,
      _id: createBinaryId(),
      _collection: Collections.DISCARD_PILES,
      cardIds: [],
    } as DiscardPile,
  });
  await litbaseClient.litql.queryTypeOnce(Collections.DRAW_DECKS, {
    $push: {
      _id: createBinaryId(),
      gameId,
      cards: cards
        .filter((elem) => elem.type !== CardType.BABY)
        .map((elem) => ({
          cardId: elem._id,
          inDeck: true,
        })),
    } as DrawDeck,
  });
}

export async function deleteGame(gameId: BinaryId) {
  await litbaseClient.litql.queryTypeOnce(Collections.GAMES, {
    $match: { _id: gameId },
    $pull: 1,
  });
  for (const collection of [Collections.HANDS, Collections.DRAW_DECKS, Collections.DISCARD_PILES]) {
    await litbaseClient.litql.queryTypeOnce(collection, {
      $matchAll: { gameId },
      $pull: 1,
    });
  }
}
