import { litbaseClient } from "../../../litbase-client";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { Hand } from "@unstable-unicorns/common/models/hand";
import { BinaryId } from "@litbase/core";
import { DrawDeck } from "@unstable-unicorns/common/models/draw-deck";
import { Card } from "@unstable-unicorns/common/models/card";
import { Game } from "@unstable-unicorns/common/models/game";

export async function drawCard({
  drawDeck,
  card,
  userId,
  game,
}: {
  drawDeck: DrawDeck;
  card: Card;
  userId: BinaryId;
  game: Game;
}) {
  await Promise.all([
    litbaseClient.litql.queryTypeOnce(Collections.DRAW_DECKS, {
      $match: { _id: drawDeck._id },
      $update: {
        $set: {
          cards: drawDeck?.cards.map((elem) => (elem.cardId === card._id ? { ...elem, inDeck: false } : elem)),
        },
      },
    }),
    litbaseClient.litql.queryTypeOnce<Hand>(Collections.HANDS, {
      $match: {
        userId: userId,
        gameId: game._id,
      },
      $update: {
        $push: {
          cardIds: card._id,
        },
      },
    }),
    litbaseClient.litql.queryTypeOnce(Collections.GAMES, {
      $match: {
        _id: game._id,
      },
      $update: {
        $set: {
          activePlayerIndex: ((game.activePlayerIndex || 0) + 1) % game.userIds.length,
        },
      },
    }),
  ]);
}
