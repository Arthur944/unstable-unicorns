import styled from "styled-components";
import { spacing72 } from "@litbase/alexandria";

export function CardPlayingArea() {
  return <Body></Body>;
}

const Body = styled.div`
  width: ${spacing72};
`;
