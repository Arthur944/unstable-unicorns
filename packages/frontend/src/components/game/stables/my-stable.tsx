import styled from "styled-components";
import { spacing24, spacing32, spacing4, spacing40, spacing48, spacing8 } from "@litbase/alexandria";
import { useQueryType, useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { useUser } from "../../../hooks/use-user";
import { useGame } from "../game-context";
import { CardPlace } from "../style-components";
import { Card } from "@unstable-unicorns/common/models/card";
import { Stable } from "@unstable-unicorns/common/models/stable";
import { forMap } from "@litbase/alexandria/utils/util-functions";
import { DiscardPile } from "@unstable-unicorns/common/models/discard-pile";
import { Game } from "@unstable-unicorns/common/models/game";
import { DragEvent, useMemo, useRef, useState } from "react";
import { usePlayersHand } from "../../../hooks/use-players-hand";
import { discardCard } from "../actions/discard-card";
import { playCard } from "../actions/play-cards";
import { toast, ToastContainer } from "react-toastify";

export function MyStable() {
  const [cardToPlay, setCardToPlay] = useState<Card | null>(null);
  const dropZoneRef = useRef<HTMLDivElement | null>(null);
  const user = useUser();
  const { gameId } = useGame();
  const handCards = usePlayersHand(user._id, gameId);
  const [stable] = useQueryTypeOne<Stable>(Collections.STABLES, {
    $match: {
      gameId,
      playerId: user._id,
    },
  });
  const [stableCards] = useQueryType<Card>(Collections.CARDS, {
    $matchAll: {
      _id: { $in: stable?.cardIds || [null] },
    },
  });

  const orderedStableCards = useMemo(() => {
    return (
      stable?.cardIds.map((elem) => stableCards.find((card) => elem.equals(card._id))).filter((elem) => !!elem?._id) ||
      []
    );
  }, [stable?.cardIds, stableCards]);

  const [discardPile] = useQueryTypeOne<DiscardPile>(Collections.DISCARD_PILES, {
    $match: {
      gameId,
    },
  });
  const [game] = useQueryTypeOne<Game>(Collections.GAMES, {
    $match: {
      _id: gameId,
    },
  });

  async function handleOnDrop(): Promise<void> {
    if (!stable || !user || !game || !cardToPlay) {
      throw new Error("Some required values are missing");
    }
    if (!["Magic Card", "Instant Card", "Upgrade Card", "Downgrade Card"].includes(cardToPlay.type)) {
      await playCard({ stable, card: cardToPlay, userId: user._id, game });
    } else {
      toast.error(`You cannot play ${cardToPlay.type} cards`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    setCardToPlay(null);
  }

  function handleOnDragStart(e: DragEvent<HTMLDivElement>, elem: Card): void {
    setCardToPlay(elem);
  }

  function handleOnDragOver(e: DragEvent<HTMLDivElement>) {
    e.preventDefault();
  }

  return (
    <Body>
      <StableCards ref={dropZoneRef} onDrop={handleOnDrop} onDragOver={handleOnDragOver}>
        {orderedStableCards.map((elem: Card | undefined) => elem && <StableCard card={elem} key={elem._id.value} />)}
        {forMap(0, 6 - stableCards.length, (index) => (
          <CardPlace key={index} />
        ))}
      </StableCards>
      <HandBody>
        {handCards?.map((elem) => (
          <HandCardComponent
            onClick={async () => {
              if (!discardPile || !user || !game) {
                throw new Error("Some required values are missing");
              }
              await discardCard({ discardPile, card: elem, userId: user._id, game });
            }}
            key={elem._id.value}
            elem={elem}
            image={elem.image}
            handleOnDragStart={handleOnDragStart}
          />
        ))}
      </HandBody>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </Body>
  );
}

function HandCardComponent({
  image,
  onClick,
  elem,
  handleOnDragStart,
}: {
  image: string;
  onClick: () => void;
  elem: Card;
  handleOnDragStart: (e: DragEvent<HTMLDivElement>, elem: Card) => void;
}) {
  return (
    <HandCardBody>
      <HandCard
        onClick={() => {
          onClick();
        }}
        style={{ backgroundImage: `url("${image}")` }}
        draggable={true}
        onDragStart={(e: DragEvent<HTMLDivElement>) => handleOnDragStart(e, elem)}
      />
    </HandCardBody>
  );
}

const bigCardWidth = "16rem";
const bigCardHeight = "23rem";

function StableCard({ card }: { card: Card }) {
  return <StableCardBody style={{ backgroundImage: `url("${card.image}")` }} />;
}

const StableCardBody = styled(CardPlace)`
  mix-blend-mode: normal;
  background-size: cover;
`;

const HandCard = styled.div`
  width: ${spacing32};
  height: ${spacing48};
  background-size: cover;
  border: 1px solid ${({ theme }) => theme.gray400};
  border-radius: ${({ theme }) => theme.roundedLg};
  cursor: pointer;
  transition: width 0.3s ease, height 0.3s ease, bottom 0.3s ease;
  position: absolute;
  bottom: 0;
`;

const StableCards = styled.div`
  display: flex;
  margin: 0 auto;
`;

const HandBody = styled.div`
  display: flex;
  align-items: flex-end;
  position: relative;
  top: ${spacing8};
  height: ${spacing40};
  margin: auto auto 0;
  overflow-x: visible;
  width: 35rem;
`;

const Body = styled.div`
  background: rgba(196, 196, 196, 0.24);
  padding: ${spacing4};
  padding: ${spacing4} ${spacing4} 0;
  display: flex;
  flex-direction: column;
  width: fit-content;
  margin: 0 auto;
  overflow-y: hidden;
  overflow-x: visible;
`;

const HandCardBody = styled.div`
  position: relative;
  width: ${spacing24};
  transition: width 0.3s ease, height 0.3s ease, bottom 0.3s ease;
  bottom: 0;
  &:hover {
    width: ${bigCardWidth};
    height: ${bigCardHeight};
    > ${HandCard} {
      width: ${bigCardWidth};
      height: ${bigCardHeight};
      bottom: ${spacing8};
      z-index: 200;
    }
  }
`;
