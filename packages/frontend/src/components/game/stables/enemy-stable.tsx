import styled from "styled-components";
import {
  spacing1,
  spacing12,
  spacing16,
  spacing18,
  spacing2,
  spacing20,
  spacing24,
  spacing32,
  spacing4,
  spacing8,
} from "@litbase/alexandria";
import cardBack from "../../../assets/images/card-back.png";
import { forMap } from "@litbase/alexandria/utils/util-functions";
import { User } from "@unstable-unicorns/common/models/user";
import { useQueryType, useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { useGame } from "../game-context";
import { Hand } from "@unstable-unicorns/common/models/hand";
import { Stable } from "@unstable-unicorns/common/models/stable";
import { Card } from "@unstable-unicorns/common/models/card";

type DeckPosition = "left" | "right" | "top";

export function EnemyStable({
  orientation,
  position,
  player,
}: {
  orientation: "horizontal" | "vertical";
  position: DeckPosition;
  player: User;
}) {
  const { gameId } = useGame();
  const [hand] = useQueryTypeOne<Hand>(Collections.HANDS, {
    $match: {
      userId: player._id,
      gameId: gameId,
    },
  });
  const [stable] = useQueryTypeOne<Stable>(Collections.STABLES, {
    $match: {
      playerId: player._id,
      gameId: gameId,
    },
  });

  const [stableCards] = useQueryType<Card>(Collections.CARDS, {
    $matchAll: {
      _id: { $in: stable?.cardIds || [null] },
    },
  });
  const cardsInHand = hand?.cardIds.length || 0;
  return (
    <Body $isVertical={orientation === "vertical"} $position={position}>
      <HandBody $position={position}>
        {forMap(0, cardsInHand, (index) => (
          <CardBody $isVertical={orientation === "vertical"} $position={position} key={index} />
        ))}
      </HandBody>
      {stableCards.map((elem) => (
        <StableCardBody
          $isVertical={orientation === "vertical"}
          style={{ backgroundImage: `url("${elem.image}")` }}
          key={elem._id.value}
        />
      ))}
      {forMap(0, 6 - stableCards.length, (index) => (
        <CardPlace $isVertical={orientation === "vertical"} key={index} />
      ))}
    </Body>
  );
}

const cardWidth = spacing12;
const cardHeight = spacing18;

const CardBody = styled.div<{ $isVertical: boolean; $position: DeckPosition }>`
  background: url("${cardBack}");
  background-size: cover;
  border-radius: ${({ theme }) => theme.roundedLg};
  width: ${spacing24};
  height: ${spacing32};
  box-shadow: ${({ theme }) => theme.shadow};
  border: 1px solid ${({ theme }) => theme.gray400};
  position: absolute;
  &:nth-child(2) {
    ${({ $isVertical }) => ($isVertical ? `top: ${spacing4}` : `left: ${spacing4}`)}
  }
  &:nth-child(3) {
    ${({ $isVertical }) => ($isVertical ? `top: ${spacing8}` : `left: ${spacing8}`)}
  }
  &:nth-child(4) {
    ${({ $isVertical }) => ($isVertical ? `top: ${spacing12}` : `left: ${spacing12}`)}
  }
  &:nth-child(5) {
    ${({ $isVertical }) => ($isVertical ? `top: ${spacing16}` : `left: ${spacing16}`)}
  }
  &:nth-child(6) {
    ${({ $isVertical }) => ($isVertical ? `top: ${spacing20}` : `left: ${spacing20}`)}
  }
  &:nth-child(7) {
    ${({ $isVertical }) => ($isVertical ? `top: ${spacing24}` : `left: ${spacing24}`)}
  }
  ${({ $isVertical }) => ($isVertical ? "transform: rotate(90deg);" : "")}
  ${({ $position }) => {
    switch ($position) {
      case "left":
        return `left: -${spacing16}`;
      case "top":
        return `top: -${spacing16}`;
      case "right":
        return `right: -${spacing16}`;
    }
  }}
`;

export const HandBody = styled.div<{ $position: DeckPosition }>`
  position: absolute;
  height: 100%;
  display: flex;
  flex-direction: column;
  ${({ $position }) => {
    switch ($position) {
      case "left":
        return `left: 0;`;
      case "right":
        return `right: 0;`;
      case "top":
        return `left: 25%`;
    }
  }};
`;

const CardPlace = styled.div<{ $isVertical: boolean }>`
  height: ${({ $isVertical }) => (!$isVertical ? cardHeight : cardWidth)};
  width: ${({ $isVertical }) => (!$isVertical ? cardWidth : cardHeight)};
  border: 1px solid ${({ theme }) => theme.gray200};
  border-radius: ${({ theme }) => theme.roundedLg};
  margin: ${spacing1};
`;

const StableCardBody = styled(CardPlace)`
  background-size: cover;
`;

const Body = styled.div<{ $isVertical: boolean; $position: DeckPosition }>`
  display: flex;
  flex-direction: ${({ $isVertical }) => ($isVertical ? "column" : "row")};
  background: rgba(196, 196, 196, 0.24);
  border-radius: ${({ theme }) => theme.rounded};
  padding: ${spacing2};
  height: fit-content;
  width: fit-content;
  overflow: hidden;
  position: relative;
  ${({ $position }) => {
    switch ($position) {
      case "left":
        return `padding-left: ${spacing16}`;
      case "top":
        return `height: 100%;align-items: flex-end;`;
      case "right":
        return `width: 100%;`;
    }
  }}
`;
