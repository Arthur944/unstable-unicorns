import { ReactNode } from "react";
import styled from "styled-components";
import { EnemyStable } from "./enemy-stable";
import { spacing5, spacing8 } from "@litbase/alexandria";
import { User } from "@unstable-unicorns/common/models/user";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { useQueryType } from "@litbase/react/hooks/use-query";
import { useGame } from "../game-context";
import { useUser } from "../../../hooks/use-user";

export function EnemyStables({ children }: { children: ReactNode }) {
  const { userIds } = useGame();
  const player = useUser();
  const [enemyPlayers] = useQueryType<User>(Collections.USERS, {
    $matchAll: {
      // We need null in the list here because mongodb doesn't like $in queries with empty lists
      _id: { $in: userIds || [null], $ne: player._id },
    },
  });
  return (
    <Body>
      <TopRow>
        {enemyPlayers.slice(0, 3).map((elem) => (
          <EnemyStable key={elem._id.value} orientation={"horizontal"} position={"top"} player={elem} />
        ))}
      </TopRow>
      <MainRow>
        <Col>
          {enemyPlayers.slice(3, 5).map((elem) => (
            <EnemyStable orientation={"vertical"} position={"left"} player={elem} key={elem._id.value} />
          ))}
        </Col>
        {children}
        <Col>
          {enemyPlayers.slice(5).map((elem) => (
            <EnemyStable orientation={"vertical"} position={"right"} player={elem} key={elem._id.value} />
          ))}
        </Col>
      </MainRow>
    </Body>
  );
}

const Col = styled.div`
  display: flex;
  flex-direction: column;
  > * + * {
    margin-top: ${spacing8};
  }
`;

const MainRow = styled.div`
  display: grid;
  grid-template-columns: 1fr 80% 1fr;
`;

const TopRow = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: flex-end;
  padding-bottom: ${spacing5};
`;

const Body = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 20% 80%;
  height: 100%;
`;
