import cardBack from "../../assets/images/card-back.png";
import { DeckBody, DeckCard, SecondaryCard } from "./deck";
import { useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { useGame } from "./game-context";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { useEffect, useMemo, useState } from "react";
import { DrawDeck } from "@unstable-unicorns/common/models/draw-deck";
import { forMap } from "@litbase/alexandria/utils/util-functions";
import styled, { css } from "styled-components";
import { Game } from "@unstable-unicorns/common/models/game";
import { useUser } from "../../hooks/use-user";
import { Card } from "@unstable-unicorns/common/models/card";
import { sample } from "lodash-es";
import { BinaryId } from "@litbase/core";
import { drawCard } from "./actions/draw-card";

export function DrawDeckComponent() {
  const [showTopCard, setShowTopCard] = useState(false);
  const [topCardClicked, setTopCardClicked] = useState(false);
  const { gameId } = useGame();
  const user = useUser();
  const [game] = useQueryTypeOne<Game>(Collections.GAMES, {
    $match: {
      _id: gameId,
    },
  });

  useEffect(() => {
    setTopCardClicked(false);
  }, [game?.activePlayerIndex]);

  const [drawDeck] = useQueryTypeOne<DrawDeck>(Collections.DRAW_DECKS, {
    $match: {
      gameId,
    },
  });

  const topCardId = useMemo(() => {
    return (sample(drawDeck?.cards.filter((elem) => elem.inDeck) || []) as { cardId: BinaryId })?.cardId;
  }, [drawDeck]);

  const [topCard] = useQueryTypeOne<Card>(Collections.CARDS, {
    $match: {
      _id: topCardId,
    },
  });

  const deckCardIds = useMemo(() => {
    return (drawDeck?.cards || []).filter((elem) => elem.inDeck).map((elem) => elem.cardId);
  }, [drawDeck]);

  const canInteract = !!(
    game?.activePlayerIndex !== undefined &&
    game?.userIds[game?.activePlayerIndex || 0]?.equals(user._id) &&
    !topCardClicked
  );

  return (
    <DeckBody>
      <FlippableCard
        showFace={showTopCard}
        canInteract={canInteract}
        src={topCard?.image || ""}
        wasClicked={topCardClicked}
        onClick={async () => {
          if (!showTopCard) {
            setShowTopCard(true);
          } else {
            if (!drawDeck || !topCard || !user || !game) {
              throw new Error("Some required values are missing");
            }
            await drawCard({ drawDeck, card: topCard, userId: user._id, game });
            setTopCardClicked(true);
            setTimeout(() => {
              setShowTopCard(false);
            }, 1000);
          }
        }}
      />
      {forMap(0, Math.min(deckCardIds.length - 1, 3), (index) => (
        <SecondaryCard style={{ backgroundImage: `url("${cardBack}")` }} key={index} />
      ))}
    </DeckBody>
  );
}

function FlippableCard({
  src,
  canInteract,
  showFace,
  onClick,
  wasClicked,
}: {
  src: string;
  canInteract: boolean;
  showFace: boolean;
  onClick: () => void;
  wasClicked: boolean;
}) {
  return (
    <FlipCard $wasClicked={wasClicked} $canInteract={canInteract} $showFace={showFace} onClick={() => onClick()}>
      <FlipCardInner>
        <FlipCardFront />
        <FlipCardBack style={{ backgroundImage: `url("${src}")` }} />
      </FlipCardInner>
    </FlipCard>
  );
}

const FlipCardFront = styled.div`
  background-image: url("${cardBack}");
  background-size: cover;
`;

const FlipCardBack = styled.div`
  transform: rotateY(180deg);
  background-size: cover;
`;

const FlipCardInner = styled.div`
  background-size: cover;
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.8s;
  transform-style: preserve-3d;
  border-radius: ${({ theme }) => theme.roundedLg};
  div {
    border-radius: ${({ theme }) => theme.roundedLg};
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden; /* Safari */
    backface-visibility: hidden;
  }
`;

const FlipCard = styled(DeckCard)<{ $canInteract: boolean; $showFace: boolean; $wasClicked: boolean }>`
  background-color: transparent;
  perspective: 1000px; /* Remove this if you don't want the 3D effect */
  border-radius: ${({ theme }) => theme.roundedLg};
  ${({ $showFace }) =>
    $showFace &&
    css`
      ${FlipCardInner} {
        transform: rotateY(180deg) translateX(15rem) scale(1.5);
      }
      cursor: pointer !important;
    `}
  ${({ $wasClicked }) =>
    $wasClicked &&
    css`
      ${FlipCardInner} {
        transform: rotateY(180deg) translateX(15rem) scale(1.5) translateY(150vh);
      }
    `}
  transform-origin: left bottom;
  transition: transform 0.3s;
  cursor: not-allowed;
  &:hover {
    ${({ $canInteract, $showFace }) =>
      $canInteract &&
      !$showFace &&
      css`
        cursor: pointer;
        transform: rotate(-10deg) translateY(-10px);
      `};
  }
  ${({ $canInteract }) =>
    !$canInteract &&
    css`
      transition: none;
      * {
        transition: none;
      }
    `}
`;
