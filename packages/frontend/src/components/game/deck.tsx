import { spacing48, spacing64 } from "@litbase/alexandria";
import styled from "styled-components";

export const DeckCard = styled.div`
  width: 100%;
  height: 100%;
  background-size: cover;
  border-radius: ${({ theme }) => theme.roundedXl};
  border: 1px solid ${({ theme }) => theme.gray400};
  box-shadow: ${({ theme }) => theme.shadowLg};
  position: absolute;
  left: 6px;
  z-index: 3;
`;

export const SecondaryCard = styled(DeckCard)`
  left: 0;
  top: 0;
  z-index: 0;
  & + & {
    left: 2px;
    z-index: 1;
  }
  & + & + & {
    left: 4px;
    z-index: 2;
  }
`;

export const DeckBody = styled.div`
  width: ${spacing48};
  height: ${spacing64};
  border-radius: ${({ theme }) => theme.roundedXl};
  position: relative;
`;
