import { DeckBody, DeckCard } from "./deck";
import { useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { useGame } from "./game-context";
import { DiscardPile } from "@unstable-unicorns/common/models/discard-pile";
import { CardPlace } from "./style-components";
import styled from "styled-components";
import { Card } from "@unstable-unicorns/common/models/card";

export function DiscardDeck() {
  const { gameId } = useGame();

  const [discardPile] = useQueryTypeOne<DiscardPile>(Collections.DISCARD_PILES, {
    $match: {
      gameId: gameId,
    },
    $live: true,
    gameId: 1,
    cardIds: 1,
  });

  const topCardId = discardPile?.cardIds?.[discardPile.cardIds.length - 1];

  const [topCard] = useQueryTypeOne<Card>(Collections.CARDS, {
    $match: {
      _id: topCardId,
    },
  });

  return (
    <DeckBody>
      {!topCard && <StyledCardPlace />}
      {topCard && <DeckCard style={{ backgroundImage: `url("${topCard.image}")` }} />}
    </DeckBody>
  );
}

const StyledCardPlace = styled(CardPlace)`
  width: 100%;
  height: 100%;
`;
