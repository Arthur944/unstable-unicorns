import { createContext, useContext } from "react";
import { BinaryId } from "@litbase/core";

export interface GameState {
  gameId: BinaryId;
  userIds: BinaryId[];
}

// @ts-ignore
export const GameContext = createContext<GameState>({});

export function useGame() {
  return useContext(GameContext);
}
