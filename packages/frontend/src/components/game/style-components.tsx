import styled from "styled-components";
import { spacing28, spacing3, spacing40 } from "@litbase/alexandria";

export const CardPlace = styled.div`
  width: ${spacing28};
  height: ${spacing40};
  border: 1px solid ${({ theme }) => theme.gray200};
  background: ${({ theme }) => theme.gray100};
  mix-blend-mode: multiply;
  border-radius: ${({ theme }) => theme.roundedXl};
  box-shadow: ${({ theme }) => theme.shadowInner};
  & + & {
    margin-left: ${spacing3};
  }
`;
