import { useUser } from "../../hooks/use-user";
import { useGame } from "./game-context";
import { useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { Game } from "@unstable-unicorns/common/models/game";
import styled from "styled-components";
import { Button } from "../inputs/button";
import { litbaseClient } from "../../litbase-client";

export function Starter() {
  const { gameId } = useGame();
  const user = useUser();
  const [game] = useQueryTypeOne<Game>(Collections.GAMES, { $match: { _id: gameId } });

  if (game?.startedAt) {
    return null;
  }
  const isUserTheHost = game && game?.creatorId.equals(user._id);
  return (
    <Body>
      {isUserTheHost ? (
        <Button
          onClick={() =>
            litbaseClient.litql.queryTypeOnce(Collections.GAMES, {
              $match: {
                _id: gameId,
              },
              $update: {
                $set: {
                  startedAt: new Date(),
                  activePlayerIndex: 0,
                },
              },
            })
          }
        >
          Start Game
        </Button>
      ) : (
        <Message>Waiting for host to start the game...</Message>
      )}
    </Body>
  );
}

const Message = styled.span`
  color: white;
  font-weight: bold;
  font-size: ${({ theme }) => theme.text3xl};
`;

const Body = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 999;
`;
