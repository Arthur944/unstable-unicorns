import { useObservable } from "@litbase/use-observable";
import { litbaseClient } from "../litbase-client";
import { emptyUser } from "@litbase/core";
import { User } from "@unstable-unicorns/common/models/user";

export function useUser() {
  const [user] = useObservable(() => litbaseClient.currentUser$, emptyUser);
  return user as User;
}
