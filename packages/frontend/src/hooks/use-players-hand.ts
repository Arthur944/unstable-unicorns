import { BinaryId } from "@litbase/core";
import { Hand } from "@unstable-unicorns/common/models/hand";
import { Collections } from "@unstable-unicorns/common/models/collections";
import { Card } from "@unstable-unicorns/common/models/card";
import { useObservable } from "@litbase/use-observable";
import { litbaseClient } from "../litbase-client";
import { filter, map, switchMap } from "rxjs/operators";

export function usePlayersHand(userId: BinaryId, gameId: BinaryId): Card[] {
  const [results] = useObservable(() => {
    return litbaseClient.litql
      .queryTypeOne<Hand>(Collections.HANDS, {
        $match: {
          userId,
          gameId,
        },
      })
      .pipe(
        filter((elem) => !!elem),
        switchMap((hand) => {
          if (!hand) {
            throw new Error(
              "This won't happen because null values have been filtered out 2 lines before, but we need this error for typechecking"
            );
          }
          return litbaseClient.litql
            .queryType<Card>(Collections.CARDS, {
              $matchAll: {
                _id: { $in: hand?.cardIds || [null] },
              },
            })
            .pipe(
              map((cards) =>
                hand.cardIds
                  .map((elem) => {
                    return cards.find((card) => card._id.equals(elem));
                  })
                  .filter((elem) => !!elem)
              )
            );
        }),
        filter((results) => results.length !== 0)
      );
  }, []);
  return results as Card[];
}
