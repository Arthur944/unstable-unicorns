import { baseTheme } from "@litbase/alexandria";

export const theme = {
  ...baseTheme,
  fontFamilyBase: "Ostrich Sans",
};
