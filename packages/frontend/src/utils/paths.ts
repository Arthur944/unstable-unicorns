export enum Paths {
  GAME = "/game",
  LOGIN = "/login",
  REGISTER = "/register",
  MAIN = "/",
}
