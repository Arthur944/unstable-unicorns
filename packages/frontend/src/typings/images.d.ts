declare module "*.jpg" {
  const value: string;
  export default value;
}

declare module "*.png" {
  const value: string;
  export default value;
}

declare module "*.svg" {
  import { ComponentType } from "react";
  const value: ComponentType;
  export default value;
}
