import { defineConfig } from "vite";
import reactRefresh from "@vitejs/plugin-react-refresh";
// @ts-ignore
import reactSvgPlugin from "vite-plugin-react-svg";
import * as path from "path";
// @ts-ignore
import dotenvFlow from "dotenv-flow";

dotenvFlow.config({ path: "../.." });

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh(), reactSvgPlugin({ defaultExport: "component" })],
  resolve: {
    alias: {
      "@unstable-unicorns/common": path.resolve(__dirname, "../common/src/"),
    },
    dedupe: ["react", "@fullcalendar/common", "@fullcalendar/daygrid", "@fullcalendar/react"],
  },
  optimizeDeps: {
    exclude: ["@fullcalendar/core", "@fullcalendar/daygrid", "@fullcalendar/react"],
  },
  esbuild: {
    jsxInject: `import React from 'react'`,
  },
  define: {
    "process.release": { ...process.release, name: "notnode" },
    "process.env": { ...process.env, NODE_ENV: "dev" },
  },
  publicDir: "./three/",
});
