module.exports = function (api) {
  const config = {
    presets: [
      [
        "@babel/preset-env",
        {
          targets: "last 1 firefox versions, last 1 chrome versions",
          loose: true,
          useBuiltIns: "usage",
          corejs: { version: 3 },
          shippedProposals: true,
        },
      ],
      [
        "@babel/preset-react",
        { /*useBuiltIns: true,*/ runtime: "automatic", useSpread: true, development: !api.env("production") },
      ],
      [
        "@babel/preset-typescript",
        { isTSX: true, allExtensions: true, allowDeclareFields: true, onlyRemoveTypeImports: true },
      ],
    ],
    plugins: [
      ["@babel/plugin-proposal-decorators", { /*decoratorsBeforeExport: true,*/ legacy: true }],
      ["@babel/plugin-proposal-class-properties", { loose: true }],
      ["@babel/plugin-proposal-optional-chaining", { loose: true }],
      ["@babel/plugin-proposal-nullish-coalescing-operator", { loose: true }],
    ],
  };

  return config;
};
