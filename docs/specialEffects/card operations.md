#card operations

#Magical Unicorns
##enter stable
###may
- steal an upgrade card
- pull card from other player
- force other player to DISCARD card
- destroy an upgrade or sacrifice a downgrade
- search deck for upgrade + add to hand then shuffle deck
- add magic card from discard pile to hand
- add Unicorn from discard to hand
- Basic Unicorn from hand to your stable
- after discard a card steal a unicorn card
- search deck for downgrade and add to hand -> shuffle deck
- sacrifice THIS card then destroy unicorn
- may add neigh from discard to hand
- search deck for a card with "Narwahl", add to hand and shuffle deck
###must
- every player sacrifice Unicorn
- draw card
- each player MUST discard a card
- sacrifice all Downgrade in own stable
- draw 2 cards and discard one

##in stable at beginning
###may
- sacrifice this card -> Unicorn from discard into stable
- discard card -> Baby Unicorn from Nursery to stable
- destroy unicorn card -> instant end of turn
- discard a unicorn card then choose a unicorn from discard pile and bring it directly to stable && immediate end of turn
###must
##in stable

###must 
- this card cannot be destroyed by Magic cards
- this card cannot be sacrifice or destroyed
- Basic Unicorn cards cannot enter stables except yours

##sacrifice or destruction

###may 
- destroy unicorn card
- discard a card instead

###must
- move to owner's hand
- counts for 2 unicorns
- player cannot play any neigh
- return to hand

##destruction of unicorn in stable
###may
- sacrifice this card
##any player's turn beginning
- move card to that player's stable
#Magic card
- move other players card from stable to hand. that player MUST discard a card
- look at other player's hand. choose a card and add it to own hand
- draw x cards and discard y cards
- then take another turn
- one card in each stable must return to hand
- move cards from one stable to another
- shuffle discard pile into deck


#Instant card
- neigh
- mega neigh
 
#Upgrade card

##can only enter when
- there is a basic unicorn
## in stable at beginning

###may
- play 2 cards during action phase
- draw 2 cards
- sacrifice a card to destroy a card
- bring basic unicorn directly into the stable
- DISCARD 2 Unicorn cards. If you do, bring a Unicorn card directly from the discard pile into your Stable.
- STEAL a Unicorn card. At the end of your turn, return that Unicorn card to the Stable from which you stole it.

##in stable
- your unicorn cards cannot be destroyed
- Cards you play cannot be Neigh'd.

#Downgrade card

##each time unicorn enters or leaves stable
- discard a card

##in stable
- all unicorns are basic unicorns
- cannot play upgrade cards
- cannot play neigh cards
- hand is visible
- all your unicorns are pandas, Cards that affect Unicorn cards do not affect your Pandas.
- If at any time you have more than 5 Unicorns in your Stable, SACRIFICE a Unicorn card.

##in stable at beginning
- SACRIFICE a Unicorn card, then DRAW a card.


