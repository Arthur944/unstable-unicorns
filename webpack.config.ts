import { Configuration, DefinePlugin, WebpackPluginInstance } from "webpack";
import os from "os";
// import MiniCssExtractPlugin from "mini-css-extract-plugin";
import path from "path";

import HtmlWebpackPlugin from "html-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import NodemonPlugin from "nodemon-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import CircularDependencyPlugin from "circular-dependency-plugin";
// @ts-ignore
import Dotenv from "dotenv-flow-webpack";
import findUp from "find-up";

import ReactRefreshWebpackPlugin from "@pmmmwh/react-refresh-webpack-plugin";
// @ts-ignore
import { WebpackPnpExternals } from "webpack-pnp-externals";
import WebpackNodeExternals from "webpack-node-externals";

process.env["TS_NODE_PROJECT"] = "";

export interface ConfigFactoryOptions {
  htmlOptions?: HtmlWebpackPlugin.Options;
  outputOptions: Configuration["output"];
  entry?: string;
  useSvgLoader?: boolean;
  isTargetingNode?: boolean;
  useSourcemap?: boolean;
}

export interface ConfigEnvironmentVariables {
  useIp?: string | boolean;
  NODE_ENV?: string;
  VERSION?: string;
  production?: boolean;
  reactDevTools?: boolean;
  analyzer?: boolean;
  useSourcemap?: boolean;
  noNodemon?: boolean;
}

export function getConfigFactory(
  factoryOptions: ConfigFactoryOptions | ((env: ConfigEnvironmentVariables) => ConfigFactoryOptions)
) {
  return function configFactory(envs: ConfigEnvironmentVariables = {}) {
    for (const [key] of Object.entries(envs)) {
      if (key.includes("=")) {
        const [actualkey, actualValue] = key.split("=");
        (envs as Record<string, string>)[actualkey] = actualValue;
      }
    }

    let {
      useIp = false,
      // eslint-disable-next-line prefer-const
      NODE_ENV = "dev",
      // eslint-disable-next-line prefer-const
      VERSION = "noversion",
      // eslint-disable-next-line prefer-const
      production = false,
      // eslint-disable-next-line prefer-const
      reactDevTools = false,
      // eslint-disable-next-line prefer-const
      analyzer = false,
      // eslint-disable-next-line prefer-const
      useSourcemap: useSourcemapEnv,
      // eslint-disable-next-line prefer-const
      noNodemon = false,
    } = envs;

    const env = { useIp, NODE_ENV, production, reactDevTools, analyzer };

    const {
      htmlOptions,
      outputOptions,
      useSvgLoader,
      isTargetingNode,
      entry,
      useSourcemap: useSourcemapOptions,
    } = typeof factoryOptions === "function" ? factoryOptions(env) : factoryOptions;

    const useSourcemap = useSourcemapEnv ?? useSourcemapOptions;

    if (useIp) {
      breakLoop: for (const networkInterfaces of Object.values(os.networkInterfaces())) {
        if (!networkInterfaces) continue;

        for (const networkInterface of networkInterfaces) {
          if (
            networkInterface.internal ||
            networkInterface.family !== "IPv4" ||
            !networkInterface.address.startsWith("192.168.")
          )
            continue;

          useIp = networkInterface.address;
          console.warn("Using local IP: " + useIp);

          break breakLoop;
        }
      }

      if (useIp) throw new Error(`Could not determine local ip`);
    }

    const babelLoader = {
      loader: "babel-loader",
      options: {
        // cacheDirectory: true,
        envName: production ? "production" : "development",
      },
    };

    const publicPath = outputOptions?.publicPath as string | undefined;

    const config: Configuration = {
      mode: production ? "production" : "development",
      optimization: {
        nodeEnv: false,
      },
      entry: entry ?? "./src/index.tsx",
      // Currently we need to add '.ts' to the resolve.extensions array.
      resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        alias: {
          "@unstable-unicorns/common/src": path.resolve("../common/src"),
          "@unstable-unicorns/common": path.resolve("../common/src"),
        },
        // "esm2017" is added so we import the smaller version of firebase/firestore
        // FIXME: Currently firestore esm2017 is buggy, so restore this when it gets fixed
        // mainFields: ["esm2017", "browser", "module", "main"],
        // plugins: [new TsconfigPathsPlugin()]
        fallback: {
          querystring: false,
          util: false,
          crypto: false,
        },
      },
      output: outputOptions,
      devServer: {
        publicPath,
        stats: "minimal",
        host: (useIp as string) ?? undefined,
        historyApiFallback: {
          index: publicPath ? publicPath.replace(/\/+$/, "") + "/index.html" : "/index.html",
          disableDotRule: true,
        },
        contentBase: path.join(__dirname, "packages/web/three"),
        hot: true,
        overlay: {
          warnings: true,
          errors: true,
        },
        compress: true,
      },
      stats: "minimal",

      // Add the loader for .ts files.
      module: {
        rules: [
          {
            test: /\.js$/,
            enforce: "pre",
            use: [
              {
                loader: "source-map-loader",
                options: {
                  filterSourceMappingUrl(url: string, resourcePath: string) {
                    if (/utility-types|@react-spring-shared/.test(resourcePath)) {
                      return "skip";
                    }

                    return true;
                  },
                },
              },
            ],
          },
          {
            test: /\.jsx$/,
            include: /\.yarn/,
            use: [
              // {
              //     loader: "thread-loader",
              //     options: {
              //         poolRespawn: false,
              //     },
              // },
              babelLoader,
            ],
          },
          {
            test: /\.[tj]sx?$/,
            exclude: /node_modules|\.yarn/,
            use: [
              // {
              //     loader: "thread-loader",
              //     options: {
              //         poolRespawn: false,
              //     },
              // },
              babelLoader,
            ],
          },
          {
            test: /\.(png|jpg|gif|webp|ico|eot|ttf|woff2|mp4|webm|AppImage|stl|ini)$/,
            type: "asset/resource",
          },
        ],
      },
      plugins: [
        new CircularDependencyPlugin({
          exclude: /node_modules/,
          failOnError: true,
        }),
        new DefinePlugin({
          CONNECT_REACT_DEVTOOLS: reactDevTools.toString(),
          HREF_BASE_NAME: JSON.stringify(outputOptions?.publicPath ?? "/"),
          "process.env.PRODUCTION": JSON.stringify(production),
          "process.env.NODE_ENV": JSON.stringify(NODE_ENV),
          "process.env.VERSION": JSON.stringify(VERSION),
        }),
        new Dotenv({
          node_env: NODE_ENV,
          path: path.dirname(findUp.sync([".env.local", ".env." + NODE_ENV, ".env"]) ?? "./.env"),
          system_vars: true,
        }),
        new CleanWebpackPlugin(),
      ],
    };

    if (!config.module || !config.plugins || !config.resolve || !config.resolve.alias) {
      throw new Error("Something went seriously wrong.");
    }

    if (useSourcemap) {
      config.devtool = "source-map";
    }

    if (isTargetingNode) {
      config.target = "node";
      config.devtool = "source-map";
      config.externals = [
        WebpackPnpExternals({
          exclude: [/lodash-es/, /^@unstable-unicorns/, /\.(png|jpg|gif|webp|ico|eot|ttf|woff2|mp4|webm)$/],
        }),
        WebpackNodeExternals({
          additionalModuleDirs: ["../../node_modules", "lodash-es"],
          allowlist: [/^@unstable-unicorns/, "lodash-es"],
        }),
      ];
      config.plugins.unshift(
        new CopyPlugin({
          patterns: [
            {
              from: "package.json",
              to: "package.json",
              transform(content: string | Buffer) {
                if (content instanceof Buffer) content = content.toString();

                // Remove scripts, so postinstall doesn't run on the server
                const { scripts, ...parsedContent } = JSON.parse(content);

                return JSON.stringify(parsedContent);
              },
            },
            "../../yarn.lock",
          ],
        })
      );

      if (!noNodemon) {
        config.plugins.push(
          new CleanWebpackPlugin(),
          new NodemonPlugin({
            nodeArgs: ["--trace-deprecation", "--inspect" /*, '--inspect-brk'*/],
            script: undefined,
            exec: "cd dist && yarn node ./index.js",
          })
        );
      }
    } else {
      config.module.rules?.push({
        test: /\.css$/,
        use: [
          /*production ? MiniCssExtractPlugin.loader :*/ "style-loader",
          { loader: "css-loader", options: { sourceMap: true } },
        ],
      });

      if (!useSvgLoader) {
        config.module.rules?.push({
          test: /\.(?<!inline\.)svg$/,
          type: "asset/resource",
        });
      }

      config.plugins.unshift(new HtmlWebpackPlugin({ ...htmlOptions, environment: { useIp, production, NODE_ENV } }));

      if (production) {
        // config.plugins.push(new MiniCssExtractPlugin());

        if (analyzer && config.plugins) {
          config.plugins.push(new BundleAnalyzerPlugin() as WebpackPluginInstance);
        }
      } else {
        // Check if we're running in serve mode. If not, don't enable the refresh plugin.
        // Workaround for bug: https://github.com/pmmmwh/react-refresh-webpack-plugin/issues/289
        // Based on: https://stackoverflow.com/a/57474397
        if (process.env.WEBPACK_DEV_SERVER || process.argv.includes("serve")) {
          config.plugins.push(new ReactRefreshWebpackPlugin());
        }
      }

      if (useSvgLoader) {
        config.module.rules?.push({
          test: /\.svg$/,
          use: [
            babelLoader,
            {
              loader: "react-svg-loader",
              options: {
                jsx: true,
              },
            },
          ],
        });
      }
    }

    return config;
  };
}
